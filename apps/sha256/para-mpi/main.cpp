#include <mpi.h>
#include <stdlib.h>
#include <iostream>
#include "sha256.h"
#include <vector>
#include <pthread.h>
#include <stdio.h>
#include <fstream>
#include <time.h>
#include <algorithm>
#include <utmpx.h>
#include <math.h>
#include <chrono>

using namespace std;
using std::string;
using std::cout;
using std::endl;
using std::chrono::steady_clock;

#define TIMECHECK
//#define CORRCHECK

#define MASTER 0
#define NUM_THREADS 3
#define NUM_WORKERS 3
int RANGE = 1000;
string input = "grape";
ofstream myfile;



int main(int argc, char** argv)
{
  if (argc != 2) {
    cout << "Please input size " << endl;
    exit(123);
  }
  int size = atoi(argv[1]);

  int provided;
  MPI_Init_thread(NULL, NULL, MPI_THREAD_MULTIPLE, &provided);
  //MPI_Init();

#ifdef CORRCHECK
  printf("Main started\n");
#endif

  if (provided != MPI_THREAD_MULTIPLE)
  {
    printf("Sorry, this MPI implementation does not support multiple threads\n");
  }
  int rank = MPI::COMM_WORLD.Get_rank();
  int instances = MPI::COMM_WORLD.Get_size();
  char name[256];
  int len;
  MPI::Get_processor_name(name, len);

#ifdef CORRCHECK
  //printf("Hi I'm %s on core %d:%d\n", name, sched_getcpu(),rank);
  myfile.open ("mpi-log");
#endif

/////////////////////////////////////////////////////////////
/* MASTER */
/////////////////////////////////////////////////////////////
  if (rank == MASTER) {

    // Generate a bunch of outputs
    srand(time(NULL));
    int genkeys[size];
    int crackedkeys[size];
    vector<string> outputs;
    for (int i = 0; i < size; i ++) {
      genkeys[i] = rand() % RANGE;
      outputs.push_back(sha256gen(input, genkeys[i]));
    }

#ifdef TIMECHECK
  auto start = steady_clock::now();
#endif
  
    // Divide work ranges. First divide into (NUM_WORKERS + 1) shares
    int p_size;
    p_size = (size + NUM_WORKERS + 1 - 1) / (NUM_WORKERS + 1);
    int partial_sizes[(NUM_WORKERS+1)*2];
    for (int i = 0; i < NUM_WORKERS + 1; i ++) {
      partial_sizes[i*2] = p_size * i;
      partial_sizes[i*2+1] = min(partial_sizes[i*2] + p_size, size);
    }

    /*
    // start threads to receive results from other nodes (machines)
    pthread_t threads[NUM_WORKERS];
    int threadIds[NUM_WORKERS];
    for (int i = 0; i < NUM_WORKERS; i++) {
      threadIds[i] = i;
      pthread_create(&threads[i], NULL, recvResponse, (void*)(int64_t)i);
    }*/

#ifdef CORRCHECK
    cout << "Start sending work" << endl;
    myfile << "Start sending work\n";
#endif

    // send to each worker, the sizes they have to deal with
    for (int dest = 1; dest < instances; dest++) {
      MPI::COMM_WORLD.Send(&partial_sizes[(dest)*2], 2, MPI::INT, dest, 0);
    }
    // send to each worker, the strings they have to compare with
    for (int dest = 1; dest < instances; dest++) {
      for (int i = partial_sizes[dest*2]; i < partial_sizes[dest*2+1]; i++) {
        MPI::COMM_WORLD.Send(outputs[i].c_str(), outputs[i].length(), MPI_CHAR, dest, 0);
      }
    }

#ifdef CORRCHECK
    cout << "Send done" << endl;
    myfile << "Send done\n";
#endif

    // Master do its own share of work, partial_hilo[0,1]
    for (int i = partial_sizes[0]; i < partial_sizes[1]; i++)
      crackedkeys[i] = sha256crack(input, outputs[i]);
    
    /*
    // wait for work to be done
    for (int i = 0; i < 3; i++ ) {
      pthread_join(threads[i], NULL);
    }*/

#ifdef CORRCHECK
    cout << "Master work done" << endl;
    myfile << "Master work done\n";
#endif
    // wait for work to be done
    MPI::Status status;
    for (int recvNode = 1; recvNode < NUM_WORKERS+1; recvNode++)
      MPI::COMM_WORLD.Recv(&crackedkeys[partial_sizes[recvNode*2]], partial_sizes[recvNode*2+1]-partial_sizes[recvNode*2], \
        MPI::INT, recvNode, MPI::ANY_TAG, status);

#ifdef CORRCHECK
    cout << "Master receives all resuls" << endl;
    myfile << "Master receives all resuls\n";
#endif

#ifdef TIMECHECK
  auto end = steady_clock::now();
  //double elapsed_secs = double(t2 - t1); // / CLOCKS_PER_SEC;
  double elapsed_secs = ((end - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
  cout<<"Time used in secs:" << elapsed_secs <<endl;
#endif

#ifdef CORRCHECK
    myfile << "Master finshed work\n";
    for (int i = 0; i < size; i++) 
      if (genkeys[i] != crackedkeys[i]) {
        cout << "cracking not correct" << endl;
        exit(345);
      }
    cout << "cracking correct" << endl;
#endif
    
  }

/////////////////////////////////////////////////////////////
/* WORKER */
/////////////////////////////////////////////////////////////
  else { 

    //Receive work from master
    MPI::Status status;

    int worker_partial_sizes[2];
    MPI::COMM_WORLD.Recv(worker_partial_sizes, 2, MPI::INT, 0, MPI_ANY_TAG, status); //receive bounds

#ifdef CORRCHECK
    //cout << worker_partial_sizes[0] << " " << worker_partial_sizes[1] << endl;
    //cout << "I am " << rank << endl;
#endif

    vector<string> outputs;
    int crackedkeys[worker_partial_sizes[1]-worker_partial_sizes[0]];

    for (int i = worker_partial_sizes[0]; i < worker_partial_sizes[1]; i++){
      MPI::COMM_WORLD.Probe(MASTER, MPI::ANY_TAG, status); //0 means MASTER
      int l = status.Get_count(MPI::CHAR);
      char *buf = new char[l];
      MPI::COMM_WORLD.Recv(buf, l, MPI::CHAR, 0, MPI::ANY_TAG, status);
      string bla1(buf, l);
      outputs.push_back(bla1);
      delete [] buf;
    }

#ifdef CORRCHECK
      cout << rank <<" finishes receiving work" << endl;
#endif
    //Do work
    for (int i = 0; i < worker_partial_sizes[1]-worker_partial_sizes[0]; i++) {
      crackedkeys[i] = sha256crack(input, outputs[i]);
    }

    //recvArrPtrs[i] = (int*)malloc(sizeof(int) * sizes[i]);
    /*if (recvArrPtrs[i] == NULL) {
      cout << "Malloc failed" << endl;
      MPI_Abort(MPI_COMM_WORLD, 666);
      exit(1);
    }*/

    /*
    int worker_hilo[2];
    MPI::COMM_WORLD.Recv(worker_hilo, 2, MPI::INT, 0, MPI_ANY_TAG, status);
    */

    //}
    //vector<int> vectorArr[bucketsPerNode];

    //Do work
    /*for (int i = 0; i < bucketsPerNode; i++) {
      //vector<int> v(recvArrPtrs[i], &(recvArrPtrs[i][sizes[i]]));
      //std::sort(&recvArrPtrs[i][0], &recvArrPtrs[i][sizes[i]] );
      quickSort(&recvArrPtrs[i][0], 0, sizes[i]-1);
    }*/

    /*int worker_result = 0;
    for (int i = worker_hilo[0]; i < worker_hilo[1]; i++) 
      if (isPrime(i)) worker_result ++;*/
    
    //Send back results
    //for (int i = 0; i < bucketsPerNode; i++) {

    //MPI::COMM_WORLD.Send(&worker_result, 1, MPI::INT, 0, 0);
#ifdef CORRCHECK
    cout << rank << " finishes work, ready to send back"  << endl;
#endif

    MPI::COMM_WORLD.Send(&crackedkeys, worker_partial_sizes[1]-worker_partial_sizes[0], MPI::INT, 0, 0);

#ifdef CORRCHECK
  cout << "sent from node " << rank <<endl;
    myfile << "sent from node " << rank <<endl;
#endif
    //}

  }
  MPI::Finalize();
  myfile.close();
  
  return 0;
}