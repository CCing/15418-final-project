#include <mpi.h>
#include <stdlib.h>
#include <iostream>
#include "sha256.h"
#include <vector>
#include <pthread.h>
#include <stdio.h>
#include <fstream>
#include <time.h>
#include <algorithm>
#include <utmpx.h>
#include <math.h>
#include <chrono>

using namespace std;
using std::string;
using std::cout;
using std::endl;
using std::chrono::steady_clock;


#define TIMECHECK
//#define CORRCHECK
int RANGE = 1000;

int main(int argc, char *argv[])
{
	if (argc !=2) {
		cout << "please put a size argument" << endl;
		exit(123);
	}
    string input = "grape";
	int size = atoi(argv[1]);
	srand(time(NULL));
	int genkeys[size];
	int crackedkeys[size];
	vector<string> outputs;
	for (int i = 0; i < size; i ++) {
		genkeys[i] = rand() % RANGE;
		outputs.push_back(sha256gen(input, genkeys[i]));
	}

#ifdef TIMECHECK
  auto start = steady_clock::now();
#endif

	for (int i = 0; i < size; i++) {
		crackedkeys[i] = sha256crack(input, outputs[i]);
	}

    //int genkey = 123;
    //string output1 = sha256gen(input, genkey);
    //int cracked = sha256crack(input, output1);

#ifdef TIMECHECK
  auto end = steady_clock::now();
  //double elapsed_secs = double(t2 - t1); // / CLOCKS_PER_SEC;
  double elapsed_secs = ((end - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
  cout<<"Time used in secs:" << elapsed_secs <<endl;
#endif

#ifdef CORRCHECK
	for (int i = 0; i < size; i++) 
		if (crackedkeys[i] != genkeys[i]) {
			cout << "cracking not correct" << endl;
			exit(10);
		}
	cout << "cracking correct" << endl;
#endif

    return 0;
}