#include <mpi.h>
#include <stdlib.h>
#include <iostream>
#include "sha256.h"
#include <vector>
#include <pthread.h>
#include <stdio.h>
#include <fstream>
#include <time.h>
#include <algorithm>
#include <utmpx.h>
#include <math.h>
#include <chrono>

using namespace std;
using std::string;
using std::cout;
using std::endl;
using std::chrono::steady_clock;

#define TIMECHECK
//#define CORRCHECK

#define MASTER 0
#define NUM_THREADS 3
#define NUM_WORKERS 3
int RANGE = 1000;
string input = "grape";
ofstream myfile;

//Master Global Vars
int *crackedkeys;
vector<string> outputs;
int *partial_sizes;

//Worker Global Vars
vector<string> Woutputs;
int *Wcrackedkeys;
int *worker_partial_sizes;

#ifdef CORRCHECK
mutex mtx;
#endif

void *threadFn(void *args) {
  int left = ((long long *)args)[0];
  int right = ((long long *)args)[1];
  int tid = ((long long *)args)[2];
  //int *crackedkeys = ((int **)args)[2];
  //vector<string> outputs = ((vector<string> *)args)[3];
  int recvNode = tid+1;

#ifdef CORRCHECK
  mtx.lock();
  //cout << "In charge of " << left << " " << right << endl;
  mtx.unlock();
#endif

    // send to each worker, the sizes they have to deal with
    //for (int dest = 1; dest < instances; dest++) {
  int dest = recvNode;
      MPI::COMM_WORLD.Send(&partial_sizes[(dest)*2], 2, MPI::INT, dest, 0);
    //}
    // send to each worker, the strings they have to compare with
    //for (int dest = 1; dest < instances; dest++) {
      for (int i = partial_sizes[dest*2]; i < partial_sizes[dest*2+1]; i++) {
        MPI::COMM_WORLD.Send(outputs[i].c_str(), outputs[i].length(), MPI_CHAR, dest, 0);
      }
    //}


  for (int i = left; i < right; i++)
    crackedkeys[i] = sha256crack(input, outputs[i]);


  MPI::Status status;
  //for (int recvNode = 1; recvNode < NUM_WORKERS+1; recvNode++)

  MPI::COMM_WORLD.Recv(&crackedkeys[partial_sizes[recvNode*2]], partial_sizes[recvNode*2+1]-partial_sizes[recvNode*2], \
    MPI::INT, recvNode, MPI::ANY_TAG, status);

  return NULL;
}


void *wthreadFn(void *args) {
  int left = ((int *)args)[0];
  int right = ((int *)args)[1];
  //int *Wcrackedkeys = ((int **)args)[2];
  //vector<string> *wtOutputs = ((vector<string> **)args)[3];
  //int *crackedkeys = ((int **)args)[2];
  //vector<string> outputs = ((vector<string> *)args)[3];
  //int recvNode = tid+1;
#ifdef CORRCHECK
  mtx.lock();
  cout << "In charge of " << left << " " << right << endl;
  mtx.unlock();
#endif
/*
#ifdef CORRCHECK
  mtx.lock();
  cout << "In charge of " << left << " " << right << endl;
  mtx.unlock();
#endif
*/

/*#ifdef CORRCHECK
  mtx.lock();
  cout << "start in worker thread" << endl;
#endif*/
  for (int i = left; i < right; i++) {
    /*
#ifdef CORRCHECK
    cout << "try writing to Wcrackedkeys" << endl;
    cout << Wcrackedkeys << endl;
    Wcrackedkeys[i] = 0;
    cout << "write to Wcrackedkeys okay " << endl;
#endif
    */
#ifdef CORRCHECK
    if (i-worker_partial_sizes[0] < 0){
      cout << "<0 index " <<endl;
      exit(123);
    }
#endif
    Wcrackedkeys[i-worker_partial_sizes[0]] = sha256crack(input, Woutputs[i-worker_partial_sizes[0]]);
  }
/*#ifdef CORRCHECK
  cout << "done in worker thread " << endl;
  mtx.unlock();
#endif*/

  return NULL;
}



int main(int argc, char** argv)
{
  if (argc != 2) {
    cout << "Please input size " << endl;
    exit(123);
  }
  int size = atoi(argv[1]);

  int provided;
  MPI_Init_thread(NULL, NULL, MPI_THREAD_MULTIPLE, &provided);
  //MPI_Init();

#ifdef CORRCHECK
  //printf("Main started\n");
#endif

  if (provided != MPI_THREAD_MULTIPLE)
  {
    printf("Sorry, this MPI implementation does not support multiple threads\n");
  }
  int rank = MPI::COMM_WORLD.Get_rank();
  int instances = MPI::COMM_WORLD.Get_size();
  char name[256];
  int len;
  MPI::Get_processor_name(name, len);

#ifdef CORRCHECK
  //printf("Hi I'm %s on core %d:%d\n", name, sched_getcpu(),rank);
  myfile.open ("mpi-log");
#endif

/////////////////////////////////////////////////////////////
/* MASTER */
/////////////////////////////////////////////////////////////
  if (rank == MASTER) {

    // Generate a bunch of outputs
    srand(time(NULL));
    int genkeys[size];
    //int crackedkeys[size];
    crackedkeys = (int*)malloc(sizeof(int)*size);
#ifdef CORRCHECK
    for (int i = 0; i < size; i ++) crackedkeys[i] = -1;
#endif
    //vector<string> outputs;
    for (int i = 0; i < size; i ++) {
      genkeys[i] = rand() % RANGE;
      outputs.push_back(sha256gen(input, genkeys[i]));
    }

#ifdef TIMECHECK
  auto start = steady_clock::now();
#endif
  
    // Divide work ranges. First divide into (NUM_WORKERS + 1) shares
    int p_size;
    p_size = (size + NUM_WORKERS + 1 - 1) / (NUM_WORKERS + 1);
    //int partial_sizes[(NUM_WORKERS+1)*2];
    partial_sizes = (int*)malloc(sizeof(int) * (NUM_WORKERS+1)*2);
    for (int i = 0; i < NUM_WORKERS + 1; i ++) {
      partial_sizes[i*2] = p_size * i;
      partial_sizes[i*2+1] = min(partial_sizes[i*2] + p_size, size);
    } //WORKERS take idx 1,2,3, MASTER take 0

    // Further divide the work into 4 shares
    long long master_works[(NUM_THREADS+1) * 4];
    int master_partial_size = (partial_sizes[1] - partial_sizes[0] + NUM_THREADS + 1 - 1) / (NUM_THREADS + 1);
    for (int i = 0; i < NUM_THREADS + 1; i++) {
      master_works[i*4] = (long long)(master_partial_size * i + partial_sizes[0]);
      master_works[i*4+1] = (long long) (min((int)master_works[i*4] + master_partial_size, partial_sizes[1]));
      //master_works[i*4+2] = (long long)(&crackedkeys[(int)master_works[i*4]]);
      master_works[i*4+2] = i;
      //master_works[i*4+2] = (long long)(crackedkeys);
      //master_works[i*4+3] = (long long)(&outputs);
    } //Thread 0,1,2 take idx 0,1,2; main thread take 3
    
    // start threads to dispatch work to other threads on same machine
    pthread_t threads[NUM_WORKERS];
    for (int i = 0; i < NUM_WORKERS; i++) {
      pthread_create(&threads[i], NULL, threadFn, (void*)(&master_works[i*4]));
    }

#ifdef CORRCHECK
    //cout << "Start sending work" << endl;
    myfile << "Start sending work\n";
#endif

    // send to each worker, the sizes they have to deal with
    /*for (int dest = 1; dest < instances; dest++) {
      MPI::COMM_WORLD.Send(&partial_sizes[(dest)*2], 2, MPI::INT, dest, 0);
    }
    // send to each worker, the strings they have to compare with
    for (int dest = 1; dest < instances; dest++) {
      for (int i = partial_sizes[dest*2]; i < partial_sizes[dest*2+1]; i++) {
        MPI::COMM_WORLD.Send(outputs[i].c_str(), outputs[i].length(), MPI_CHAR, dest, 0);
      }
    }*/

#ifdef CORRCHECK
    //cout << "Send done" << endl;
    myfile << "Send done\n";
#endif

    // Master do its own share of work, partial_hilo[0,1]
    //for (int i = partial_sizes[0]; i < partial_sizes[1]; i++)
#ifdef CORRCHECK
    mtx.lock();
    //cout << "in chrge of " << master_works[NUM_THREADS*4] << " " << master_works[NUM_THREADS*4+1] << endl;
    mtx.unlock();
#endif
    for (int i = master_works[NUM_THREADS*4]; i < master_works[NUM_THREADS*4+1]; i++)
      crackedkeys[i] = sha256crack(input, outputs[i]);
    
    
    // wait for work to be done
    for (int i = 0; i < 3; i++ ) {
      pthread_join(threads[i], NULL);
    }


#ifdef CORRCHECK
    //cout << "Master work done" << endl;
    myfile << "Master work done\n";
#endif

    // wait for work to be done
    /*MPI::Status status;
    for (int recvNode = 1; recvNode < NUM_WORKERS+1; recvNode++)
      MPI::COMM_WORLD.Recv(&crackedkeys[partial_sizes[recvNode*2]], partial_sizes[recvNode*2+1]-partial_sizes[recvNode*2], \
        MPI::INT, recvNode, MPI::ANY_TAG, status);*/

#ifdef CORRCHECK
    //cout << "Master receives all resuls" << endl;
    myfile << "Master receives all resuls\n";
#endif


#ifdef TIMECHECK
  auto end = steady_clock::now();
  //double elapsed_secs = double(t2 - t1); // / CLOCKS_PER_SEC;
  double elapsed_secs = ((end - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
  cout<<"Time used in secs:" << elapsed_secs <<endl;
#endif

#ifdef CORRCHECK
    myfile << "Master finshed work\n";
    for (int i = 0; i < size; i++) 
      if (genkeys[i] != crackedkeys[i]) {
        cout << genkeys[i] << endl;
        cout << "cracking not correct" << endl;
        cout << i << endl;
        cout << crackedkeys[i] << endl;
        //exit(345);
        cout << "\n" <<endl;
      }
    cout << "cracking correct" << endl;
#endif
    
  }

/////////////////////////////////////////////////////////////
/* WORKER */
/////////////////////////////////////////////////////////////
  else { 

    //Receive work from master
    MPI::Status status;

    //int worker_partial_sizes[2];
    worker_partial_sizes = (int*)malloc(sizeof(int) * 2);
    MPI::COMM_WORLD.Recv(worker_partial_sizes, 2, MPI::INT, 0, MPI_ANY_TAG, status); //receive bounds

    cout << worker_partial_sizes[0] << " " << worker_partial_sizes[1] << endl;

#ifdef CORRCHECK
    //cout << worker_partial_sizes[0] << " " << worker_partial_sizes[1] << endl;
    //cout << "I am " << rank << endl;
#endif

    //vector<string> Woutputs;
    //int Wcrackedkeys[worker_partial_sizes[1]-worker_partial_sizes[0]];
    //int *Wcrackedkeys;
    Wcrackedkeys = (int*)malloc(sizeof(int) * (worker_partial_sizes[1]-worker_partial_sizes[0]));
    if (Wcrackedkeys == NULL) {
      cout << "Wcrackedkeys not allocated " << endl;
      exit(12345);
    }

    for (int i = worker_partial_sizes[0]; i < worker_partial_sizes[1]; i++) {
      MPI::COMM_WORLD.Probe(MASTER, MPI::ANY_TAG, status); //0 means MASTER
      int l = status.Get_count(MPI::CHAR);
      char *buf = new char[l];
      MPI::COMM_WORLD.Recv(buf, l, MPI::CHAR, 0, MPI::ANY_TAG, status);
      string bla1(buf, l);
      Woutputs.push_back(bla1);
      delete [] buf;
    }

#ifdef CORRCHECK
      //cout << rank <<" finishes receiving work" << endl;
#endif

    //Do work
    /*for (int i = 0; i < worker_partial_sizes[1]-worker_partial_sizes[0]; i++) {
      Wcrackedkeys[i] = sha256crack(input, Woutputs[i]);
    }*/

    // Further divide the work into 4 shares
    int worker_works[(NUM_THREADS+1) * 4];
    int worker_partial_size = (worker_partial_sizes[1] - worker_partial_sizes[0] + NUM_THREADS + 1 - 1) / (NUM_THREADS + 1);
    cout << "worker_partial_size is " << worker_partial_size << endl;
    for (int i = 0; i < NUM_THREADS + 1; i++) {
      worker_works[i*4] = (worker_partial_size * i + worker_partial_sizes[0]);
      worker_works[i*4+1] = (min(worker_works[i*4] + worker_partial_size, worker_partial_sizes[1]));
    } //Thread 0,1,2 take idx 0,1,2; main thread take 3

    // start threads to dispatch work to other threads on same machine
    pthread_t wthreads[NUM_THREADS];
    for (int i = 0; i < NUM_THREADS; i++) {
      pthread_create(&wthreads[i], NULL, wthreadFn, (void*)(&worker_works[i*4]));
    }

#ifdef CORRCHECK
    mtx.lock();
    cout << "Worker main starting work" << rank <<endl;
#endif

#ifdef CORRCHECK
  cout << "Inn charge of " << worker_works[NUM_THREADS*4] << " " << worker_works[NUM_THREADS*4+1] << endl;
#endif

    for (int i = worker_works[NUM_THREADS*4]; i < worker_works[NUM_THREADS*4+1]; i++) {
#ifdef CORRCHECK
      if (i - worker_partial_sizes[0] < 0) {
        cout << "@@@@@@ < 0 " <<endl;
        exit(321);
      }
#endif
      Wcrackedkeys[i - worker_partial_sizes[0]] = sha256crack(input, Woutputs[i - worker_partial_sizes[0]]);
    }
#ifdef CORRCHECK
    cout << "Worker main done " << rank  << endl;
    mtx.unlock();
#endif

    // wait for work to be done
    for (int i = 0; i < NUM_THREADS; i++ ) {
      pthread_join(wthreads[i], NULL);
    }



    MPI::COMM_WORLD.Send(Wcrackedkeys, worker_partial_sizes[1]-worker_partial_sizes[0], MPI::INT, 0, 0);

#ifdef CORRCHECK
  //cout << "sent from node " << rank <<endl;
    myfile << "sent from node " << rank <<endl;
#endif
    //}

  }
  MPI::Finalize();
  myfile.close();
  
  return 0;
}