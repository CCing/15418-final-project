#include <mpi.h>
#include <stdlib.h>
#include <iostream>
#include "sha256.h"
#include <vector>
#include <pthread.h>
#include <stdio.h>
#include <fstream>
#include <time.h>
#include <algorithm>
#include <utmpx.h>
#include <math.h>
#include <chrono>

using namespace std;
using std::string;
using std::cout;
using std::endl;
using std::chrono::steady_clock;

#define TIMECHECK
//#define CORRCHECK
#define NUM_THREADS 3
int RANGE = 1000;
string input = "grape";
int *crackedkeys;
vector<string> outputs;

void *threadFunction(void *args) {
	int startIdx, endIdx;
	startIdx = *(int*)args;
	endIdx = ((int*)args)[1];
	for (int i = startIdx; i < endIdx; i++) {
		crackedkeys[i] = sha256crack(input, outputs[i]);
	}
	return NULL;
}


int main(int argc, char *argv[])
{
	if (argc !=2) {
		cout << "please put a size argument" << endl;
		exit(123);
	}
	int size = atoi(argv[1]);
	srand(time(NULL));
	int genkeys[size];
	//int crackedkeys[size];
	crackedkeys = (int*)malloc(sizeof(int) * size);
#ifdef CORRCHECK
	for (int i = 0; i < size; i ++) crackedkeys[i] = -1;
#endif
	for (int i = 0; i < size; i ++) {
		genkeys[i] = rand() % RANGE;
		outputs.push_back(sha256gen(input, genkeys[i]));
	}

#ifdef TIMECHECK
  auto start = steady_clock::now();
#endif

	/*for (int i = 0; i < size; i++) {
		crackedkeys[i] = sha256crack(input, outputs[i]);
	}*/

	//divide work
	int pthread_sizes[(NUM_THREADS + 1) * 2];
	int each_worker_size = (size + NUM_THREADS + 1 - 1) / (NUM_THREADS + 1);
	for (int i = 0; i < NUM_THREADS+1; i++) {
		pthread_sizes[i*2] = each_worker_size * i;
		pthread_sizes[i*2+1] = min(size, pthread_sizes[i*2]+each_worker_size);
		cout << pthread_sizes[i*2] << " " << pthread_sizes[i*2+1] << endl;
	}

	pthread_t threads[NUM_THREADS];
	//int tids[NUM_THREADS];
	for (int i = 0; i < NUM_THREADS; i++ ) {
		//tids[i] = i;
		pthread_create(&threads[i], NULL, threadFunction, (void*)(&pthread_sizes[i*2]));
	}
#ifdef CORRCHECK
    //cout << pthread_sizes[NUM_THREADS] << " " << pthread_sizes[NUM_THREADS+1] << endl;
#endif
	for (int i = pthread_sizes[NUM_THREADS*2]; i < pthread_sizes[NUM_THREADS*2+1]; i++) {
		crackedkeys[i] = sha256crack(input, outputs[i]);
	}


	for (int i = 0; i < NUM_THREADS; i++ ) {
		pthread_join(threads[i], NULL);
	}

    //int genkey = 123;
    //string output1 = sha256gen(input, genkey);
    //int cracked = sha256crack(input, output1);

#ifdef TIMECHECK
  auto end = steady_clock::now();
  //double elapsed_secs = double(t2 - t1); // / CLOCKS_PER_SEC;
  double elapsed_secs = ((end - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
  cout<<"Time used in secs:" << elapsed_secs <<endl;
#endif

#ifdef CORRCHECK
	for (int i = 0; i < size; i++) 
		if (crackedkeys[i] != genkeys[i]) {
			cout << "cracking not correct" << endl;
			cout << crackedkeys[i] << " " << genkeys[i] << endl;
			exit(123);
		}
	cout << "cracking correct" << endl;
#endif

	free(crackedkeys);
    return 0;
}