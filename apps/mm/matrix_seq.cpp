//Quicksort, http://www.algolist.net/Algorithms/Sorting/Quicksort

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <vector>
#include <chrono>
#include <random>
#include <cfloat>
#include <pthread.h>
//#define ARRLEN 100000
using namespace std;
using std::chrono::steady_clock;
#define TIMECHECK
#define NUM_THREADS 4

#define M 2048
#define N 2048
#define K 2048
#define BLOCKSIZE 16

float A[M][K];
float B[K][N];
float C[M][N];


int main(int argc, char *argv[]) {
  //if (argc > 1)
  //  int BLOCKSIZE = atoi(argv[1]);
  int BLOCKSIZE_I = BLOCKSIZE;
  int BLOCKSIZE_J = BLOCKSIZE;
  int BLOCKSIZE_K = BLOCKSIZE;

  srand(time(0));
  float max = 1E+10;
  //cout << "double: " << sizeof(double) << " float: " << sizeof(float) << endl; 
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < M; j++) {
      A[i][j] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max));
      B[i][j] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max));
    }
  }
#ifdef TIMECHECK
  auto start = steady_clock::now();
  cout << "Timer start" << A[0][0] << endl;
#endif
  
  if (argc == 1) {
    cout << "normal multiply" << endl;
    // compute C += A * B
    for (int j=0; j<M; j++)
      for (int i=0; i<N; i++)
        for (int k=0; k<K; k++)
          C[j][i] += A[j][k] * B[k][i];
  } else {
    cout << "blocking multiply" << endl;
  for (int jblock=0; jblock<M; jblock+=BLOCKSIZE_J)
    for (int iblock=0; iblock<N; iblock+=BLOCKSIZE_I)
      for (int kblock=0; kblock<K; kblock+=BLOCKSIZE_K)
        for (int j=0; j<BLOCKSIZE_J; j++)
          for (int i=0; i<BLOCKSIZE_I; i++)
            for (int k=0; k<BLOCKSIZE_K; k++)
              C[jblock+j][iblock+i] += A[jblock+j][kblock+k] * B[kblock+k][iblock+i];
  }
#ifdef TIMECHECK
  auto curr = steady_clock::now();
  double elapsed_secs = ((curr - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
  cout<<"Time used in secs:" << elapsed_secs << " " << C[M-1][N-1] <<endl;
#endif



  return 0;
}
