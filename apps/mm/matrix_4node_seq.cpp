#include <mpi.h>
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <vector>
#include <chrono>
#include <random>
#include <cfloat>
#include <deque>
#include <mutex>
#include <pthread.h>
//#define ARRLEN 100000
using namespace std;
using std::chrono::steady_clock;
#define TIMECHECK
#define NUM_WORKERS 3
#define END_TAG 2333

#define M 2048
#define N 2048
#define K 2048
#define BLOCKSIZE 16
int BLOCKSIZE_I = BLOCKSIZE;
int BLOCKSIZE_J = BLOCKSIZE;
int BLOCKSIZE_K = BLOCKSIZE;

int CHUNK_SIZE = BLOCKSIZE * 1;
int NUM_CHUNKS = M / CHUNK_SIZE;

float A[M][K];
float B[K][N];
float C[M][N];

pthread_barrier_t barrier;

deque<int> workQ;

mutex m;

void *doMultiply(void *arg) {
  //int id = (int)arg;
  
  while (1) {
    m.lock();
    if (workQ.empty()) {
      m.unlock();
      continue;
    }
    int chunk = workQ.front();
    workQ.pop_front();
    m.unlock();
    
    
    int M_start = chunk * CHUNK_SIZE;
    int M_end = (chunk + 1) * CHUNK_SIZE;
    
    for (int jblock=M_start; jblock<M_end; jblock+=BLOCKSIZE_J)
      for (int iblock=0; iblock<N; iblock+=BLOCKSIZE_I)
        for (int kblock=0; kblock<K; kblock+=BLOCKSIZE_K)
          for (int j=0; j<BLOCKSIZE_J; j++)
            for (int i=0; i<BLOCKSIZE_I; i++)
              for (int k=0; k<BLOCKSIZE_K; k++)
                C[jblock+j][iblock+i] += A[jblock+j][kblock+k] * B[kblock+k][iblock+i];
    
    MPI::COMM_WORLD.Send(&(C[M_start][0]), CHUNK_SIZE * N, MPI::FLOAT, 0, chunk);
  }
  
  return 0;
}


void *recvResponse(void *arg) {
  int rank = MPI::COMM_WORLD.Get_rank();
  MPI::Status status;
  int chunkCount = 0;
  while (chunkCount < NUM_CHUNKS) {
    
    MPI::COMM_WORLD.Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, status);
    int source = status.Get_source();
    int size = status.Get_count(MPI::FLOAT);
    int tag = status.Get_tag();
    
    MPI::COMM_WORLD.Recv(&(C[tag*CHUNK_SIZE][0]), size, MPI::FLOAT, source, tag);
    chunkCount++;
  }
  
  return NULL;
}


int main(int argc, char *argv[]) {
  //if (argc > 1)
  //  int BLOCKSIZE = atoi(argv[1]);
  
  int provided;
  MPI_Init_thread(NULL, NULL, MPI_THREAD_MULTIPLE, &provided);
  //MPI_Init();
  
  if (provided != MPI_THREAD_MULTIPLE)
  {
    printf("Sorry, this MPI implementation does not support multiple threads\n");
  }
  
  pthread_barrier_init(&barrier, NULL, 4);
  
  int rank = MPI::COMM_WORLD.Get_rank();
  int size = MPI::COMM_WORLD.Get_size();
  char name[8];
  int len;
  MPI::Get_processor_name(name, len);
  
  printf("Hi I'm %s on core %d of node %d\n", name, sched_getcpu(),rank);
  
  srand(time(0));
  float max = 1E+10;
  //cout << "double: " << sizeof(double) << " float: " << sizeof(float) << endl;
  
  if (rank == 0) {
    
    
    pthread_t thread;
    pthread_create(&thread, NULL, recvResponse, (void*)0);
    
    for (int i = 0; i < M; i++) {
      for (int j = 0; j < M; j++) {
        A[i][j] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max));
        B[i][j] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max));
      }
    }
    
#ifdef TIMECHECK
    auto start = steady_clock::now();
    cout << "Timer start" << endl;
#endif
    
//    pthread_barrier_wait(&barrier);  
//    
//    int M_start = M / NUM_THREADS * 3;
//    
//    for (int jblock=M_start; jblock<M; jblock+=BLOCKSIZE_J)
//      for (int iblock=0; iblock<N; iblock+=BLOCKSIZE_I)
//        for (int kblock=0; kblock<K; kblock+=BLOCKSIZE_K)
//          for (int j=0; j<BLOCKSIZE_J; j++)
//            for (int i=0; i<BLOCKSIZE_I; i++)
//              for (int k=0; k<BLOCKSIZE_K; k++)
//                C[jblock+j][iblock+i] += A[jblock+j][kblock+k] * B[kblock+k][iblock+i];
    
    
    // Send B to worker nodes
    MPI::COMM_WORLD.Send(&B[0][0], K * N, MPI::FLOAT, 1, 0);
    MPI::COMM_WORLD.Send(&B[0][0], K * N, MPI::FLOAT, 2, 0);
    MPI::COMM_WORLD.Send(&B[0][0], K * N, MPI::FLOAT, 3, 0);
    
    int chunk = 0;
    int dest = 0;
    
    while (chunk < NUM_CHUNKS) {
      MPI::COMM_WORLD.Send(&(A[chunk * CHUNK_SIZE][0]), CHUNK_SIZE * K, MPI::FLOAT, dest % NUM_WORKERS + 1, chunk);
      chunk++;
      dest++;
    }
    
    
    
    
    
    pthread_join(thread, NULL);
    
#ifdef TIMECHECK
    auto curr = steady_clock::now();
    double elapsed_secs = ((curr - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
    cout<<"Time used in secs:" << elapsed_secs << " " << C[M-1][N-1] <<endl;
#endif
    
    int dummy = 0;
    for (int i = 0; i < NUM_WORKERS; i++) {
      MPI::COMM_WORLD.Send(&dummy, 1, MPI::INT, i+1, END_TAG);
    }
    
  } else {
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
    
    MPI::COMM_WORLD.Recv(&(B[0][0]), K * N, MPI::FLOAT, 0, MPI_ANY_TAG);
    
    
    pthread_t thread[1];
    for (int i = 0; i < 1; i++) {
      pthread_create(&thread[i], NULL, doMultiply, NULL);
    }

    
    MPI::Status status;
    while (1) {
      MPI::COMM_WORLD.Probe(0, MPI_ANY_TAG, status);
      int tag = status.Get_tag();
      if (tag == END_TAG) {
        break;
      }
      int size = status.Get_count(MPI::FLOAT);
      
      MPI::COMM_WORLD.Recv(&(A[tag * CHUNK_SIZE][0]), size, MPI::FLOAT, 0, tag);

      m.lock();
      workQ.push_back(tag);
      m.unlock();
      
    }
    
    
  }
  
  return 0;
}
