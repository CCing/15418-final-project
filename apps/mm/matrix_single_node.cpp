
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <vector>
#include <chrono>
#include <random>
#include <cfloat>
#include <pthread.h>
//#define ARRLEN 100000
using namespace std;
using std::chrono::steady_clock;
#define TIMECHECK
#define NUM_THREADS 4

#define M 2048
#define N 2048
#define K 2048
#define BLOCKSIZE 16
int BLOCKSIZE_I = BLOCKSIZE;
int BLOCKSIZE_J = BLOCKSIZE;
int BLOCKSIZE_K = BLOCKSIZE;


float A[M][K];
float B[K][N];
float C[M][N];

pthread_barrier_t barrier;

void *doMultiply(void *arg) {
  int id = (int)arg;
  int M_start = M / NUM_THREADS * id;
  int M_end = M / NUM_THREADS * (id + 1);
  
  pthread_barrier_wait(&barrier);
  
  for (int jblock=M_start; jblock<M_end; jblock+=BLOCKSIZE_J)
    for (int iblock=0; iblock<N; iblock+=BLOCKSIZE_I)
      for (int kblock=0; kblock<K; kblock+=BLOCKSIZE_K)
        for (int j=0; j<BLOCKSIZE_J; j++)
          for (int i=0; i<BLOCKSIZE_I; i++)
            for (int k=0; k<BLOCKSIZE_K; k++)
              C[jblock+j][iblock+i] += A[jblock+j][kblock+k] * B[kblock+k][iblock+i];
  
  return 0;
}

int main(int argc, char *argv[]) {
  //if (argc > 1)
  //  int BLOCKSIZE = atoi(argv[1]);

  srand(time(0));
  float max = 1E+10;
  cout << "init\n";
  //cout << "double: " << sizeof(double) << " float: " << sizeof(float) << endl;
  pthread_barrier_init(&barrier, NULL, 4);
  
  pthread_t thread[3];
  pthread_create(&thread[0], NULL, doMultiply, (void*)0);
  pthread_create(&thread[1], NULL, doMultiply, (void*)1);
  pthread_create(&thread[2], NULL, doMultiply, (void*)2);
  
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < M; j++) {
      A[i][j] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max));
      B[i][j] = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/max));
    }
  }
  
#ifdef TIMECHECK
  auto start = steady_clock::now();
  cout << "Timer start" << endl;
#endif
  
  pthread_barrier_wait(&barrier);
  
  int M_start = M / NUM_THREADS * 3;
  
  for (int jblock=M_start; jblock<M; jblock+=BLOCKSIZE_J)
    for (int iblock=0; iblock<N; iblock+=BLOCKSIZE_I)
      for (int kblock=0; kblock<K; kblock+=BLOCKSIZE_K)
        for (int j=0; j<BLOCKSIZE_J; j++)
          for (int i=0; i<BLOCKSIZE_I; i++)
            for (int k=0; k<BLOCKSIZE_K; k++)
              C[jblock+j][iblock+i] += A[jblock+j][kblock+k] * B[kblock+k][iblock+i];
  
  pthread_join(thread[0], NULL);
  pthread_join(thread[1], NULL);
  pthread_join(thread[2], NULL);
  
#ifdef TIMECHECK
  auto curr = steady_clock::now();
  double elapsed_secs = ((curr - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
  cout<<"Time used in secs:" << elapsed_secs << " " << C[M-1][N-1] <<endl;
#endif



  return 0;
}
