from subprocess import call
import sys, os

#print os.getcwd()

d = raw_input("re-make?, input y/n ")

if d == "y":
	call(["rm"]+["quickSort"])
	call(["rm"]+["builtinSort"])
	call(["g++"] + ["-o"] + ["builtinSort"]+["-std=c++11"] +["qs-seq-builtin.cpp"])
	call(["g++"] + ["-o"] + ["quickSort"] + ["-std=c++11"]+["qs-seq.cpp"])

s = raw_input("how big of array-size? ")
w = raw_input("which one to run? type: \nb for builtin\nq for qs\nt for both ")

if w=="b":
	print "builtinSort takes:"
	call(["./builtinSort"] + [s])
if w=="q":
	print "qs takes:"
	call(["./quickSort"] + [s])
if w=="t":
	print "builtinSort takes:"
	call(["./builtinSort"] + [s])
	print "qs takes:"
	call(["./quickSort"] + [s])