#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <time.h>
#include <vector>
#include <deque>
#include <chrono>
#include <mutex>
#include <algorithm>
#include <utmpx.h>
#include "work_queue.h"
using namespace std;
using std::chrono::steady_clock;

#define TIMECHECK
//#define CORRCHECK
//#define ARRLEN 10000000
//#define NUM_OF_BUCKETS 24 //REQUIRE %2==0
#define NUM_WORKERS 3
#define NUM_THREADS 4
#define DEMAND_TAG 0
#define DATA_TAG 1
#define END_TAG 2

ofstream myfile;
int NUM_OF_BUCKETS;
int bucketRange; //= 1073741824 / (NUM_OF_BUCKETS / 2); // 2^31 / NUM_OF_BUCKETS
int ARRLEN;
int* arr;

vector<vector<int>> buckets;
deque<int> workerRecord[NUM_WORKERS];
deque<pair<int*, int*>> workQ;
//WorkQueue<int*> workQ;

vector<vector<short>> myBuckets[NUM_THREADS];
pthread_barrier_t barrier;

mutex m;

void quickSort(int arr[], int left, int right) {
  int i = left, j = right;
  int tmp;
  int pivot = arr[(left + right) / 2];
  
  /* partition */
  while (i <= j) {
    while (arr[i] < pivot)
      i++;
    while (arr[j] > pivot)
      j--;
    if (i <= j) {
      tmp = arr[i];
      arr[i] = arr[j];
      arr[j] = tmp;
      i++;
      j--;
    }
  };
  
  /* recursion */
  if (left < j)
    quickSort(arr, left, j);
  if (i < right)
    quickSort(arr, i, right);
}



void *recvResponse(void *arg) {
  int rank = MPI::COMM_WORLD.Get_rank();
  //printf("Hi I'm receiver thread on core %d of node %d\n", sched_getcpu(),rank);
  //int recvNode = (int)(int64_t)arg + 1;
  MPI::Status status;
  /* Assuming round robin static allocation, and response in the same order as sending data.
  for (int i = recvNode - 1; i < NUM_OF_BUCKETS / NUM_WORKERS * NUM_WORKERS; i += NUM_WORKERS) {
    
    MPI::COMM_WORLD.Recv(&(buckets[i][0]), buckets[i].size(), MPI::INT, recvNode, MPI::ANY_TAG, status);
    myfile << "Received from node "<< recvNode << endl;
  }*/
  
  int bucketCount = 0;
  while (bucketCount < NUM_OF_BUCKETS) {
    //cout << "probing" << endl;
    MPI::COMM_WORLD.Probe(MPI_ANY_SOURCE, DATA_TAG, status);
    int source = status.Get_source();
    //cout << "Probed data from node " << source << endl;
    int size = status.Get_count(MPI::INT);
    m.lock();
    int bucketI = workerRecord[source-1].front();
    workerRecord[source-1].pop_front();
    m.unlock();
    //cout << "Get bucket id " << bucketI << endl;
    MPI::COMM_WORLD.Recv(&(buckets[bucketI][0]), size, MPI::INT, source, DATA_TAG);
    //cout << "one bucket done" << endl;
    //workerRecord[source].pop_front();
    bucketCount++;
  }
  
  return NULL;
}

void *doBuckets(void* args) {
  int id = (int)args;
  //vector<short> myBuckets[NUM_OF_BUCKETS];
  int chunk = ARRLEN / NUM_THREADS;
  
  pthread_barrier_wait(&barrier);
  
  for (int i = id * chunk; i < (id + 1) * chunk; i++) {
    int value = arr[i];
    int bucketI = value / bucketRange;
    if (bucketI >= NUM_OF_BUCKETS) bucketI = NUM_OF_BUCKETS-1;
    myBuckets[id][bucketI].push_back((short)value);
  }
  
  pthread_barrier_wait(&barrier);
  int parts = NUM_OF_BUCKETS / NUM_THREADS;
  for (int i = id * parts; i < (id+1) * parts; i++) {
    for (int j = 0; j < NUM_THREADS; j++) {
      buckets[i].insert(buckets[i].end(), myBuckets[j][i].begin(), myBuckets[j][i].end());
    }
  }
  
  pthread_barrier_wait(&barrier);
  
  if (id == 0)
    recvResponse(NULL);
  
  return NULL;
}


void *doWork(void *arg) {
  int rank = MPI::COMM_WORLD.Get_rank();
  //printf("Hi I'm worker thread on core %d of node %d\n", sched_getcpu(),rank);
  while (1) {
    m.lock();
    if (workQ.empty()) {
      m.unlock();
      continue;
    }
    pair<int*, int*> p = workQ.front();
    workQ.pop_front();
    m.unlock();
    //int* start = workQ.get_work();
    //int* end = workQ.get_work();
    int* start = p.first;
    int* end = p.second;
    
    std::sort(start, end);
    
    MPI::COMM_WORLD.Send(start, end-start, MPI::INT, 0, DATA_TAG);
    
    free(start);
  }
  
}

int main(int argc, char** argv)
{
  if (argc != 3) {
    printf("Expect ARRLEN and NUM_OF_BUCKETS as arguments!\n");
    exit(1);
  }
  ARRLEN = atoi(argv[1]);
  NUM_OF_BUCKETS = atoi(argv[2]);
  bucketRange = 1073741824 / (NUM_OF_BUCKETS / 2);
  
  int provided;
  MPI_Init_thread(NULL, NULL, MPI_THREAD_MULTIPLE, &provided);
  //MPI_Init();

  if (provided != MPI_THREAD_MULTIPLE)
  {
    printf("Sorry, this MPI implementation does not support multiple threads\n");
  }
  
  pthread_barrier_init(&barrier, NULL, 4);
  
  int rank = MPI::COMM_WORLD.Get_rank();
  int size = MPI::COMM_WORLD.Get_size();
  char name[8];
  int len;
  MPI::Get_processor_name(name, len);
  
  printf("Hi I'm %s on core %d of node %d\n", name, sched_getcpu(),rank);
  
  myfile.open ("mpi-log");
  
  clock_t t1,t2;
  
  /*MPI::Finalize();
  myfile.close();
  
  return 0;*/
  int bucketsPerNode = NUM_OF_BUCKETS / (size - 1);;
  int dummy = 0;
  // Master
  if (rank == 0) {
    //buckets = (vector<int> *)malloc(sizeof(vector<int>) * NUM_OF_BUCKETS);
    buckets.resize(NUM_OF_BUCKETS, *(new vector<int>));
    for (int i = 0; i < NUM_THREADS; i++) {
      myBuckets[i].resize(NUM_OF_BUCKETS, *(new vector<short>));
    }
    // Initialize array and buckets
    srand (time(0));
    arr = (int *)malloc(sizeof(int) * ARRLEN);
    for (int i = 0; i < ARRLEN; i++) {
      arr[i] = rand();
    }
    
    pthread_t thread[3];
    pthread_create(&thread[0], NULL, doBuckets, (void*)0);
    pthread_create(&thread[1], NULL, doBuckets, (void*)1);
    pthread_create(&thread[2], NULL, doBuckets, (void*)2);
    
    
#ifdef TIMECHECK
//    clock_t t1,t2;
//    double elapsed_secs;
//    t1=clock();
    cout << "timer started " << endl;
    auto start = steady_clock::now();
#endif
    
    /* Divide to chunks
    for (int i = 0; i < ARRLEN; i++) {
      int value = arr[i];
      int bucketI = value / bucketRange;
      if (bucketI >= NUM_OF_BUCKETS) bucketI = NUM_OF_BUCKETS-1;
      buckets[bucketI].push_back(value);
    }*/
    
    // parallel divide
    pthread_barrier_wait(&barrier);
    int chunk = ARRLEN / NUM_THREADS;
    for (int i = 3 * chunk; i < 4 * chunk; i++) {
      int value = arr[i];
      int bucketI = value / bucketRange;
      if (bucketI >= NUM_OF_BUCKETS) bucketI = NUM_OF_BUCKETS-1;
      myBuckets[3][bucketI].push_back((short)value);
    }
    
    pthread_barrier_wait(&barrier);
    
    free(arr);
    
    int parts = NUM_OF_BUCKETS / NUM_THREADS;
    for (int i = 3 * parts; i < 4 * parts; i++) {
      for (int j = 0; j < NUM_THREADS; j++) {
        buckets[i].insert(buckets[i].end(), myBuckets[j][i].begin(), myBuckets[j][i].end());
      }
    }
    
    pthread_barrier_wait(&barrier);
    
    for (int i = 0; i < NUM_THREADS; i++) {
      for (int j = 0; j < NUM_OF_BUCKETS; j++) {
        myBuckets[i][j].clear();
      }
      myBuckets[i].clear();
    }
    
#ifdef TIMECHECK
//    t2=clock();
//    elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
    auto curr = steady_clock::now();
    double elapsed_secs = ((curr - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
    cout<<"Time used for dividing buckets:" << elapsed_secs <<endl;
#endif

    // start threads to receive results from other nodes (machines)
    /*pthread_t threads[NUM_WORKERS];
    int threadIds[NUM_WORKERS];
    for (int i = 0; i < NUM_WORKERS; i++) {
      threadIds[i] = i;
      pthread_create(&threads[i], NULL, recvResponse, (void*)(int64_t)i);
    }*/
    
    //for (int i = 0; i < NUM_OF_BUCKETS; i++) {
    //  myfile << "Bucket size: " << buckets[i].size() << endl;
    //}
    
    //myfile << "Start sending work\n";
    /* Send work, static allocation: round robin
    int bucket = 0;
    for (int i = 0; i < bucketsPerNode; i++) {
      for (int dest = 1; dest < size; dest++) {
        //printf("new bucket\n");
        int sizeToSend = buckets[bucket].size();
        //printf("send size: %d\n", sizeToSend);
        //MPI::COMM_WORLD.Send(&sizeToSend, 1, MPI::INT, dest, 0);
        //printf("send array\n");
        MPI::COMM_WORLD.Send(&(buckets[bucket][0]), sizeToSend, MPI::INT, dest, 0);
        //printf("send bucket done\n");
        bucket++;
      }
    } */
    
    // Send work, send on demand
    int bucket = 0;
    int dest = 0;
    int demand;
    MPI::Status status;
    while (bucket < NUM_OF_BUCKETS) {
      //MPI::COMM_WORLD.Recv(&demand, 1, MPI::INT, MPI_ANY_SOURCE, DEMAND_TAG, status);
      //int dest = status.Get_source();
      
      m.lock();
      workerRecord[dest % NUM_WORKERS].push_back(bucket);
      m.unlock();
      
      MPI::COMM_WORLD.Send(&(buckets[bucket][0]), buckets[bucket].size(), MPI::INT, dest % NUM_WORKERS + 1, DATA_TAG);
      //cout << "Sent bucket to node "<< dest << endl;
      bucket++;
      dest++;
    }

#ifdef TIMECHECK
//    t2=clock();
//    elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
    curr = steady_clock::now();
    elapsed_secs = ((curr - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
    cout<<"Time used after sending: " << elapsed_secs <<endl;
#endif

    //myfile << "Send done\n";
    
    /* sort the leftover buckets on master itself
    for (int i = bucketsPerNode * (size - 1); i < NUM_OF_BUCKETS; i++) {
      int bSize = buckets[i].size();
      std::sort(&buckets[i][0], &buckets[i][0] + bSize);
      //quickSort(&buckets[i][0], 0, bSize-1);
    }*/
    
    // wait for work to be done
    /*for (int i = 0; i < 3; i++ ) {
      pthread_join(threads[i], NULL);
    }*/
    
    pthread_join(thread[0], NULL);
    cout << "Received all responses\n";

    
    cout << "Sort done\n";
#ifdef TIMECHECK
//    t2=clock();
//    elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
    curr = steady_clock::now();
    elapsed_secs = ((curr - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
    cout<<"Total time used: " << elapsed_secs <<endl;
#endif
    
    //myfile << "Master finshed work\n" ;
    
    for (int i = 0; i < NUM_WORKERS; i++) {
      MPI::COMM_WORLD.Send(&dummy, 1, MPI::INT, i+1, END_TAG);
    }
    
    pthread_join(thread[1], NULL);
    pthread_join(thread[2], NULL);
    
#ifdef CORRCHECK
    cout << "Checking for correctness..." << endl;

    int b = 0;
    int j = 0;
    std::sort(&arr[0], &arr[ARRLEN]);
    for (int i = 0; i < ARRLEN; i++) {
      if (buckets[b][j] != arr[i]) {
        cout << "NOT sorted at " << i << ": " << buckets[b][j] << " vs " << arr[i] << endl;
        exit(1);
      }
      if (j == buckets[b].size() - 1) {
        b++;
        j = 0;
      } else {
      	j++;
      }
    }
    free(arr);
    cout << "All sorted !" <<endl;
#endif
    
    
  }
  else {
    // On each worker node, there needs a thread receiving the data and another sorting the data and sending back.
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
#ifdef TIMECHECK
    //clock_t t1,t2;
    //double elapsed_secs;
    //t1=clock();
#endif
    
    /*
    MPI::Status status;
    int sizes[bucketsPerNode];
    int *recvArrPtrs[bucketsPerNode];
    for (int i = 0; i < bucketsPerNode; i++) {
      //MPI::COMM_WORLD.Recv(&sizes[i], 1, MPI::INT, 0, MPI_ANY_TAG, status);
      MPI::COMM_WORLD.Probe(0, MPI_ANY_TAG, status);
      sizes[i] = status.Get_count(MPI::INT);
      recvArrPtrs[i] = (int*)malloc(sizeof(int) * sizes[i]);
      if (recvArrPtrs[i] == NULL) {
        cout << "Malloc failed" << endl;
        MPI_Abort(MPI_COMM_WORLD, 666);
        exit(1);
      }
      MPI::COMM_WORLD.Recv(recvArrPtrs[i], sizes[i], MPI::INT, 0, MPI_ANY_TAG, status);
    }

    for (int i = 0; i < bucketsPerNode; i++) {
      std::sort(&recvArrPtrs[i][0], &recvArrPtrs[i][sizes[i]] );
      //quickSort(&recvArrPtrs[i][0], 0, sizes[i]-1);
    }
    
    for (int i = 0; i < bucketsPerNode; i++) {
      MPI::COMM_WORLD.Send(recvArrPtrs[i], sizes[i], MPI::INT, 0, 0);
      myfile << "sent from node " << rank <<endl;
    } */
    
    pthread_t thread[3];
    for (int i = 0; i < 3; i++) {
      pthread_create(&thread[i], NULL, doWork, NULL);
    }
    
    MPI::Status status;
    while (1) {
      //MPI::COMM_WORLD.Send(&dummy, 1, MPI::INT, 0, DEMAND_TAG);
      MPI::COMM_WORLD.Probe(0, MPI_ANY_TAG, status);
      if (status.Get_tag() == END_TAG) {
        break;
      }
      int chunkSize = status.Get_count(MPI::INT);
      int* chunk = (int*)malloc(sizeof(int) * chunkSize);
      MPI::COMM_WORLD.Recv(chunk, chunkSize, MPI::INT, 0, DATA_TAG);
      //cout << "Worker " << rank << "receivec work" << endl;
      //cout << "Worker " << rank << " chunkSize " << chunkSize << endl;
      m.lock();
      workQ.push_back(make_pair(chunk, chunk+chunkSize));
      //workQ.push_back(chunk+chunkSize);
      m.unlock();
      myfile << "Worker " << rank << "work put" << endl;
    }

    
#ifdef TIMECHECK
    //t2=clock();
    //elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
    //myfile<<"Total time used after sending back:" << elapsed_secs <<endl;
#endif
  }
  MPI::Finalize();
  myfile.close();
  
  return 0;
}
