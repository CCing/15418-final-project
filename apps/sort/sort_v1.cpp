#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <chrono>
#include <time.h>
#include <vector>
#include <algorithm>
#include <utmpx.h>
using namespace std;
using std::chrono::steady_clock;

#define TIMECHECK
//#define CORRCHECK
//#define ARRLEN 10000000
#define NUM_OF_BUCKETS 16 //REQUIRE %2==0
#define NUM_WORKERS 3

ofstream myfile;
int bucketRange = 1073741824 / (NUM_OF_BUCKETS / 2); // 2^31 / NUM_OF_BUCKETS

vector<int> buckets[NUM_OF_BUCKETS];


void quickSort(int arr[], int left, int right) {
  int i = left, j = right;
  int tmp;
  int pivot = arr[(left + right) / 2];
  
  /* partition */
  while (i <= j) {
    while (arr[i] < pivot)
      i++;
    while (arr[j] > pivot)
      j--;
    if (i <= j) {
      tmp = arr[i];
      arr[i] = arr[j];
      arr[j] = tmp;
      i++;
      j--;
    }
  };
  
  /* recursion */
  if (left < j)
    quickSort(arr, left, j);
  if (i < right)
    quickSort(arr, i, right);
}



void *recvResponse(void *arg) {
  int recvNode = (int)(int64_t)arg + 1;
  MPI::Status status;
  // Assuming round robin static allocation, and response in the same order as sending data.
  for (int i = recvNode - 1; i < NUM_OF_BUCKETS / NUM_WORKERS * NUM_WORKERS; i += NUM_WORKERS) {
    
    MPI::COMM_WORLD.Recv(&(buckets[i][0]), buckets[i].size(), MPI::INT, recvNode, MPI::ANY_TAG, status);
    myfile << "Received from node "<< recvNode << endl;
  }
  return NULL;
}


int main(int argc, char** argv)
{
  if (argv == NULL) {
    printf("Expect ARRLEN as argument!\n");
    exit(1);
  }
  int ARRLEN = atoi(argv[1]);
  int provided;
  MPI_Init_thread(NULL, NULL, MPI_THREAD_MULTIPLE, &provided);
  //MPI_Init();

  if (provided != MPI_THREAD_MULTIPLE)
  {
    printf("Sorry, this MPI implementation does not support multiple threads\n");
  }
  int rank = MPI::COMM_WORLD.Get_rank();
  int size = MPI::COMM_WORLD.Get_size();
  char name[256];
  int len;
  MPI::Get_processor_name(name, len);
  
  printf("Hi I'm %s on core %d of node %d\n", name, sched_getcpu(),rank);
  
  myfile.open ("mpi-log");
  
  clock_t t1,t2;
  
  /*MPI::Finalize();
  myfile.close();
  
  return 0;*/
  int bucketsPerNode = NUM_OF_BUCKETS / (size - 1);;

  // Master
  if (rank == 0) {
    //buckets = (vector<int> *)malloc(sizeof(vector<int>) * NUM_OF_BUCKETS);

    // Initialize array and buckets
    srand (time(0));
    int* arr = (int *)malloc(sizeof(int) * ARRLEN);
    for (int i = 0; i < ARRLEN; i++) {
      arr[i] = rand();
    }

#ifdef TIMECHECK
    //clock_t t1,t2;
    auto start = steady_clock::now();
    double elapsed_secs;
    //t1=clock();
#endif
    
    // Divide to chunks
    for (int i = 0; i < ARRLEN; i++) {
      int value = arr[i];
      int bucketI = value / bucketRange;
      if (bucketI >= NUM_OF_BUCKETS) bucketI = NUM_OF_BUCKETS-1;
      buckets[bucketI].push_back(value);
    }
    
#ifdef TIMECHECK
//    t2=clock();
//    elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
    auto curr = steady_clock::now();
    elapsed_secs = ((curr - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
    cout<<"Time used for dividing buckets:" << elapsed_secs <<endl;
#endif

#ifndef CORRCHECK
    free(arr);
#endif

    // start threads to receive results from other nodes (machines)
    pthread_t threads[NUM_WORKERS];
    int threadIds[NUM_WORKERS];
    for (int i = 0; i < NUM_WORKERS; i++) {
      threadIds[i] = i;
      pthread_create(&threads[i], NULL, recvResponse, (void*)(int64_t)i);
    }
    
    for (int i = 0; i < NUM_OF_BUCKETS; i++) {
      myfile << "Bucket size: " << buckets[i].size() << endl;
    }
    
    myfile << "Start sending work\n";
    // Send work, static allocation: round robin
    int bucket = 0;
    for (int i = 0; i < bucketsPerNode; i++) {
      for (int dest = 1; dest < size; dest++) {
        //printf("new bucket\n");
        int sizeToSend = buckets[bucket].size();
        //printf("send size: %d\n", sizeToSend);
        MPI::COMM_WORLD.Send(&sizeToSend, 1, MPI::INT, dest, 0);
        //MPI::COMM_WORLD.Send(array)
        //MPI::COMM_WORLD.Send(&(buckets[bucket].size()), 1, MPI::INT, dest, 0);
        //printf("send array\n");
        MPI::COMM_WORLD.Send(&(buckets[bucket][0]), sizeToSend, MPI::INT, dest, 0);
        //printf("send bucket done\n");
        bucket++;
      }
    }
    myfile << "Send done\n";
    
    // sort the leftover buckets on master itself
    for (int i = bucketsPerNode * (size - 1); i < NUM_OF_BUCKETS; i++) {
      int bSize = buckets[i].size();
      std::sort(&buckets[i][0], &buckets[i][0] + bSize);
      //quickSort(&buckets[i][0], 0, bSize-1);
    }
    
    // wait for work to be done
    for (int i = 0; i < 3; i++ ) {
      pthread_join(threads[i], NULL);
    }
    
    printf("Sort done\n");
#ifdef TIMECHECK
//    t2=clock();
//    elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
    curr = steady_clock::now();
    elapsed_secs = ((curr - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
    cout<<"Total time used:" << elapsed_secs <<endl;
#endif
    
    myfile << "Master finshed work\n" ;
    
#ifdef CORRCHECK
    cout << "Checking for correctness..." << endl;

    int b = 0;
    int j = 0;
    std::sort(&arr[0], &arr[ARRLEN]);
    for (int i = 0; i < ARRLEN; i++) {
      if (buckets[b][j] != arr[i]) {
        cout << "NOT sorted" << endl;
        exit(1);
      }
      if (j == buckets[b].size() - 1) {
        b++;
        j = 0;
      } else {
      	j++;
      }
    }
    free(arr);
    cout << "All sorted !" <<endl;
#endif
    
  }
  else {
    // On each worker node, there needs a thread receiving the data and another sorting the data and sending back.
//    //myfile << "Writing this to a file from worker.\n";
//    int sum = 0;
//    for(int i=0; i<ARRSIZE; i++) {
//      data[i] =  i * 3 / 3;
//      sum = sum + data[i];
//    }
//    data[ARRSIZE / 2] = 500;
//    
//    int val = rank + 10;
//    printf("%s:%d sending %d...\n", name, rank, val);
//    t1=clock(); //
//    MPI::COMM_WORLD.Send(&val, 1, MPI::INT, 0, 0);
//    t2=clock(); //
//    double elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
//    cout<<"(WOR)Time used in secs:" << elapsed_secs <<endl;
//    printf("%s:%d sent %d\n", name, rank, val);
//    
//    MPI::COMM_WORLD.Send(&data, ARRSIZE, MPI::INT, 0, 0);
//    
//    //printf("%d\n", data[ARRSIZE / 2] );
//    myfile << data[ARRSIZE / 2] ;

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
    MPI::Status status;
    int sizes[bucketsPerNode];
    int *recvArrPtrs[bucketsPerNode];
    for (int i = 0; i < bucketsPerNode; i++) {
      MPI::COMM_WORLD.Recv(&sizes[i], 1, MPI::INT, 0, MPI_ANY_TAG, status);
      recvArrPtrs[i] = (int*)malloc(sizeof(int) * sizes[i]);
      if (recvArrPtrs[i] == NULL) {
        cout << "Malloc failed" << endl;
        MPI_Abort(MPI_COMM_WORLD, 666);
        exit(1);
      }
      MPI::COMM_WORLD.Recv(recvArrPtrs[i], sizes[i], MPI::INT, 0, MPI_ANY_TAG, status);
    }
    //vector<int> vectorArr[bucketsPerNode];

    for (int i = 0; i < bucketsPerNode; i++) {
      //vector<int> v(recvArrPtrs[i], &(recvArrPtrs[i][sizes[i]]));
      std::sort(&recvArrPtrs[i][0], &recvArrPtrs[i][sizes[i]] );
      //quickSort(&recvArrPtrs[i][0], 0, sizes[i]-1);
    }
    
    for (int i = 0; i < bucketsPerNode; i++) {
      MPI::COMM_WORLD.Send(recvArrPtrs[i], sizes[i], MPI::INT, 0, 0);
      myfile << "sent from node " << rank <<endl;
    }

  }
  MPI::Finalize();
  myfile.close();
  
  return 0;
}