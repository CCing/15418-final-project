//Quicksort, http://www.algolist.net/Algorithms/Sorting/Quicksort

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <vector>
#include <chrono>
#include <algorithm>
//#define ARRLEN 100000
using namespace std;
using std::chrono::steady_clock;
#define TIMECHECK
#define CORRCHECK


int main(int argc, char *argv[]) {
  int ARRLEN = atoi(argv[1]);
  srand (time(0));
  //int random_variable = std::rand();
  //cout << random_variable << endl;

  int *arr = (int *)malloc(sizeof(int) * ARRLEN);

  for (int i = 0; i < ARRLEN; i++) 
    arr[i] = std::rand();

#ifdef TIMECHECK
//  clock_t t1,t2;
//  t1=clock();
  auto start = steady_clock::now();
#endif

  //quickSort(arr, 0, ARRLEN);
  std::sort(arr, arr+ARRLEN);

#ifdef TIMECHECK
  //t2=clock();
  //float diff ((float)t2-(float)t1);
  //double elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
  auto curr = steady_clock::now();
  double elapsed_secs = ((curr - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
  cout<<"Time used in secs:" << elapsed_secs <<endl;
#endif

#ifdef CORRCHECK
  for (int i = 0; i < ARRLEN-1; i++) {
    if (arr[i] > arr[i+1]) {
      cout << "not sorted" << endl;
      exit(1);
    }
  }
  cout << "yes, it's sorted" << endl;

#endif

  free(arr);

  return 0;
}