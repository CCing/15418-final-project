//Quicksort, http://www.algolist.net/Algorithms/Sorting/Quicksort

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <vector>
#include <chrono>
#include <algorithm>
//#define ARRLEN 100000
using namespace std;
using std::chrono::steady_clock;
#define NUM_OF_BUCKETS 16

#define TIMECHECK
#define CORRCHECK

vector<int> buckets[NUM_OF_BUCKETS];
int bucketRange = 1073741824 / (NUM_OF_BUCKETS / 2); // 2^31 / NUM_OF_BUCKETS

int main(int argc, char *argv[]) {
  int ARRLEN = atoi(argv[1]);
  srand (time(0));
  //int random_variable = std::rand();
  //cout << random_variable << endl;

  int *arr = (int *)malloc(sizeof(int) * ARRLEN);

  for (int i = 0; i < ARRLEN; i++) 
    arr[i] = std::rand();

#ifdef TIMECHECK
//  clock_t t1,t2;
//  t1=clock();
  auto start = steady_clock::now();
#endif

  // Divide to chunks
  for (int i = 0; i < ARRLEN; i++) {
    int value = arr[i];
    int bucketI = value / bucketRange;
    if (bucketI >= NUM_OF_BUCKETS) bucketI = NUM_OF_BUCKETS-1;
    buckets[bucketI].push_back(value);
  }
  
  //quickSort(arr, 0, ARRLEN);
  for (int i = 0; i < NUM_OF_BUCKETS; i++) {
    std::sort(buckets[i].begin(), buckets[i].end());
  }

#ifdef TIMECHECK
  //t2=clock();
  //float diff ((float)t2-(float)t1);
  //double elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
  auto curr = steady_clock::now();
  double elapsed_secs = ((curr - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
  cout<<"Time used in secs:" << elapsed_secs <<endl;
#endif

#ifdef CORRCHECK
  int b = 0;
  int j = 0;
  std::sort(&arr[0], &arr[ARRLEN]);
  for (int i = 0; i < ARRLEN; i++) {
    if (buckets[b][j] != arr[i]) {
      cout << "NOT sorted" << endl;
      exit(1);
    }
    if (j == buckets[b].size() - 1) {
      b++;
      j = 0;
    } else {
      j++;
    }
  }
  cout << "yes, it's sorted" << endl;

#endif

  free(arr);

  return 0;
}