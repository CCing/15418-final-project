//Quicksort, http://www.algolist.net/Algorithms/Sorting/Quicksort

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
//#define ARRLEN 100000
using namespace std;

#define TIMECHECK
#define CORRCHECK

void quickSort(int arr[], int left, int right) {
  int i = left, j = right;
  int tmp;
  int pivot = arr[(left + right) / 2];

  /* partition */
  while (i <= j) {
    while (arr[i] < pivot)
      i++;
    while (arr[j] > pivot)
      j--;
    if (i <= j) {
      tmp = arr[i];
      arr[i] = arr[j];
      arr[j] = tmp;
      i++;
      j--;
    }
  };

  /* recursion */
  if (left < j)
    quickSort(arr, left, j);
  if (i < right)
    quickSort(arr, i, right);
}

int main(int argc, char *argv[]) {
  srand (time(0));
  //int random_variable = std::rand();
  //cout << random_variable << endl;
  int ARRLEN = atoi(argv[1]);

  int *arr = (int *)malloc(sizeof(int) * ARRLEN);

  for (int i = 0; i < ARRLEN; i++) 
    arr[i] = std::rand();

#ifdef TIMECHECK
  clock_t t1,t2;
  t1=clock();
#endif

  quickSort(arr, 0, ARRLEN);

#ifdef TIMECHECK
  t2=clock();
  //float diff ((float)t2-(float)t1);
  double elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
  cout<<"Time used in secs:" << elapsed_secs <<endl;
#endif

#ifdef CORRCHECK
  for (int i = 0; i < ARRLEN-1; i++) {
    if (arr[i] > arr[i+1]) {
      cout << "not sorted" << endl;
      exit(1);
    }
  }
#endif

  free(arr);

  return 0;
}