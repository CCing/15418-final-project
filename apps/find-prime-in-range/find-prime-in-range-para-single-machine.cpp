//Inputs [lower, higher), 
//output the number of primes within this range

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <math.h>
//#include <thread>
#include "CycleTimer.h"
#include <unistd.h>
#include <chrono>

using namespace std;
using std::chrono::steady_clock;
/*struct timespec{
  time_t tv_sec;
  long tv_nsec;
};*/

struct timespec st;
struct timespec st2;


#define TIMECHECK
//#define CORRCHECK
#define NUM_THREADS 4 //rpi has 4 cores
int partial_results[NUM_THREADS];
int input_args[2];

bool isPrime(int n) {
	int i;
	int nsqrt = sqrt(n) + 1;
	for(i=2; i <= nsqrt; ++i) {
		if(n%i==0) return true;
  }
  return false;
}

void *threadFunction(void *arg) {
  int tid = *((int*)arg);
  int len = (input_args[1] - input_args[0] + NUM_THREADS - 1) / NUM_THREADS;
  int start = len * tid + input_args[0];
  int end = min(start + len, input_args[1]);

  //mtx.lock();
  //cout << input_args[0] << input_args[1] << endl;
  //cout << "thread " << tid << " eval from " << start << " to " << end << endl;
  //mtx.unlock();
  int result = 0;
  for (int i = start; i < end; i++) 
    if (isPrime(i)) 
      result++;
  partial_results[tid] = result;
  return NULL;
}

int main(int argc, char *argv[]) {
  //srand (time(0));

  if (argc != 3) {
  	cout << "Please input [lower, higer) " << endl;
  	exit(123);
  }
  int lo = atoi(argv[1]);
  int hi = atoi(argv[2]);
  input_args[0] = lo;
  input_args[1] = hi;

#ifdef TIMECHECK
  /*double t1,t2;
  //t1=CycleTimer::currentSeconds();//clock();
  gettimeofday(CLOCK_REALTIME, &st);
  time_t st1 = st.tv_sec;
  cout << "st1: " << st1 << endl;*/
  auto start = steady_clock::now();
#endif

  // do work here
  pthread_t threads[NUM_THREADS];
  int tids[NUM_THREADS];
  for (int i = 0; i < NUM_THREADS; i++ ) {
    tids[i] = i;
    pthread_create(&threads[i], NULL, threadFunction, (void*)(&tids[i]));
  }
  
  for (int i = 0; i < NUM_THREADS; i++ ) {
    pthread_join(threads[i], NULL);
  }
  int final_result = 0;
  for (int i = 0; i < NUM_THREADS; i++) 
    final_result += partial_results[i];

  cout << "Number of primes between " << lo << " and " << hi << " is " << final_result << endl;

#ifdef TIMECHECK
  //t2=CycleTimer::currentSeconds();//clock();
  /*gettimeofday(CLOCK_REALTIME, &st2);
  cout << "t2: " << t2 << endl;
  //float diff ((float)t2-(float)t1);
  */
  auto end = steady_clock::now();
  //double elapsed_secs = double(t2 - t1); // / CLOCKS_PER_SEC;
  double elapsed_secs = ((end - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
  cout<<"Time used in secs:" << elapsed_secs <<endl;
#endif

#ifdef CORRCHECK

  //TODO: correction check here

#endif

  return 0;
}