from subprocess import call
import sys, os
import time

"""
compileOrNot = raw_input("Re-compile or not? \nType y/n: ")
if "y" in compileOrNot:
	print "Will be compiled as [./runSort]"
	call(["rm", "-rf", "runSort"])
	call(["mpic++", "sort.cpp", "-o", "runSort"])
"""

with open(".send-a-file-log", "r") as fin:
	oldDest = fin.readline().strip()
	oldF = fin.readline().strip()

"""choose = raw_input("Send to [%s] with file [%s] ? \nType y/n: "% (oldDest, oldF))"""

"""if "y" == choose:
	dest = oldDest
	f = oldF
else:
	dest = raw_input("Send to which nodes? 2/3/4/5 \n ")
	f = raw_input("Which file? \n ")"""

dest = "2345"
f = "find-prime-in-range-para-mpi-pthread.cpp"

if "2" in dest:
	call(["scp", f, "pi@169.254.0.2:/home/pi/418proj"]) 
	#call(["ssh", "pi@169.254.0.2", "'cd ~/418proj; mpic++ sort.cpp -o runSort'"])
if "3" in dest:
	call(["scp", f, "pi@169.254.0.3:/home/pi/418proj"])
	#call(["ssh", "pi@169.254.0.3", "'cd ~/418proj; mpic++ sort.cpp -o runSort'"])
if "4" in dest:
	call(["scp", f, "pi@169.254.0.4:/home/pi/418proj"])
	#call(["ssh", "pi@169.254.0.4", "'cd ~/418proj; mpic++ sort.cpp -o runSort'"])
if "5" in dest:
	call(["scp", f, "pi@169.254.0.5:/home/pi/418proj"])
	#call(["ssh", "pi@169.254.0.5", "'cd ~/418proj; mpic++ sort.cpp -o runSort'"])


time.sleep(1)

if "2" in dest:
	#call(["scp", f, "pi@169.254.0.2:/home/pi/418proj"]) #not used basically
	#call(["ssh", "pi@169.254.0.2", "  'cd ~/418proj; mpic++ sort.cpp -o runSort'  "])
	call(["make", "twopmp"])
if "3" in dest:
	#call(["scp", f, "pi@169.254.0.3:/home/pi/418proj"])
	#call(["ssh", "pi@169.254.0.3", "  'cd ~/418proj; mpic++ sort.cpp -o runSort'  "])
	call(["make", "threepmp"])
if "4" in dest:
	#call(["scp", f, "pi@169.254.0.4:/home/pi/418proj"])
	#call(["ssh", "pi@169.254.0.4", "  'cd ~/418proj; mpic++ sort.cpp -o runSort'  "])
	call(["make", "fourpmp"])
if "5" in dest:
	#call(["scp", f, "pi@169.254.0.5:/home/pi/418proj"])
	#call(["ssh", "pi@169.254.0.5", "  'cd ~/418proj; mpic++ sort.cpp -o runSort'  "])
	call(["make", "fivepmp"])


with open(".send-a-file-log", "w") as fout:
	fout.write(dest)
	fout.write("\n")
	fout.write(f)