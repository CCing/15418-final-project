//Inputs [lower, higher), 
//output the number of primes within this range

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <math.h>
#include "CycleTimer.h"


using namespace std;

#define TIMECHECK
//#define CORRCHECK

bool isPrime(int n) {
	int i;
	int nsqrt = sqrt(n) + 1;
	for(i=2; i <= nsqrt; ++i) {
		if(n%i==0) return true;
  }
  return false;
}

int main(int argc, char *argv[]) {
  //srand (time(0));

  if (argc != 3) {
  	cout << "Please input [lower, higer) " << endl;
  	exit(123);
  }
  int lo = atoi(argv[1]);
  int hi = atoi(argv[2]);

#ifdef TIMECHECK
  double t1,t2;
  t1=CycleTimer::currentSeconds();//clock();
#endif

  //@TODO: do work here
  int result = 0;
  for (int i = lo; i < hi; i++)
  	if (isPrime(i)) result ++;
  cout << "Number of primes between " << lo << " and " << hi << " is " << result << endl;

#ifdef TIMECHECK
  t2=CycleTimer::currentSeconds();//clock();
  //float diff ((float)t2-(float)t1);
  double elapsed_secs = double(t2 - t1); // / CLOCKS_PER_SEC;
  cout<<"Time used in secs:" << elapsed_secs <<endl;
#endif

#ifdef CORRCHECK

  //TODO: correction check here

#endif

  return 0;
}