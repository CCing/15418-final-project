#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <time.h>
#include <vector>
#include <algorithm>
#include <utmpx.h>
#include <mutex>
#include <math.h>
#include <chrono>

using namespace std;
using std::chrono::steady_clock;

#define TIMECHECK
#define DEBUG
//#define CORRCHECK
#define NUM_WORKERS 3
#define NUM_WORKER_THREADS 3 //#threads on each worker
int partial_results[NUM_WORKERS];

ofstream myfile;
#ifdef DEBUG
mutex mtx;
#endif

bool isPrime(int n) {
  int i;
  int nsqrt = sqrt(n) + 1;
  for(i=2; i <= nsqrt; ++i) {
    if(n%i==0) return true;
  }
  return false;
}

//does its own job of computing prime, and receive answer from other machines
void *threadFn(void *arg) {
  int recvNode = *(int*)arg + 1;

  int thread_lo = ((int*)arg)[1];
  int thread_hi = ((int*)arg)[2];
#ifdef DEBUG
  mtx.lock();
  cout << "computing from " << thread_lo << " to " << thread_hi << endl;
  mtx.unlock();
#endif
  int thread_result = 0;
  for (int i = thread_lo; i < thread_hi; i++) 
    if (isPrime(i)) thread_result++;
  *(int*)arg = thread_result;

  MPI::Status status;
  // Assuming round robin static allocation, and response in the same order as sending data.
  
  if (recvNode > 0) {
    MPI::COMM_WORLD.Recv(&partial_results[recvNode-1], 1, MPI::INT, recvNode, MPI::ANY_TAG, status);
#ifdef DEBUG
    myfile << "Received from node "<< recvNode << endl;
#endif
  }

#ifdef DEBUG
  cout << "thread receiving from " << recvNode << " has exited " << endl;
#endif

  return NULL;
}


int main(int argc, char** argv)
{
  if (argc != 3) {
    cout << "Please input [lower, higer) " << endl;
    exit(123);
  }
  int lo = atoi(argv[1]);
  int hi = atoi(argv[2]);

  int provided;
  MPI_Init_thread(NULL, NULL, MPI_THREAD_MULTIPLE, &provided);
  //MPI_Init();
#ifdef DEBUG
  cout << "Main started\n" << endl;
#endif
  if (provided != MPI_THREAD_MULTIPLE)
  {
    printf("Sorry, this MPI implementation does not support multiple threads\n");
  }
  int rank = MPI::COMM_WORLD.Get_rank();
  int size = MPI::COMM_WORLD.Get_size();
  char name[256];
  int len;
  MPI::Get_processor_name(name, len);

#ifdef DEBUG
  printf("Hi I'm %s on core %d:%d\n", name, sched_getcpu(),rank);
  myfile.open ("mpi-log");
#endif

  // Master
  if (rank == 0) {

#ifdef TIMECHECK
  auto start = steady_clock::now();
#endif

    // Divide work ranges. First divide into (NUM_WORKERS + 1) shares
    int partial_hilo[(NUM_WORKERS + 1) * 2]; //([lo0, hi0],[lo1, hi1])
    int p_len;
    p_len = (hi - lo + NUM_WORKERS + 1 - 1) / (NUM_WORKERS + 1);
    for (int i = 0; i < NUM_WORKERS + 1; i ++) {
      partial_hilo[i*2] = lo + p_len * i;
      partial_hilo[i*2+1] = min(partial_hilo[i*2] + p_len, hi);
    }
    //above is dividing over machines, below is dividing over threads on one machine
    int master_thread_args[NUM_WORKERS*3]; //{[t0: tid,lo,hi], [t1], [t2]}. results written into idx0,3,6
    //used NUM_WORKERS here because NUM_WORKERS == #threads master needs to generate
    int master_thread_len = (partial_hilo[1] - partial_hilo[0] + NUM_WORKERS + 1 - 1) / (NUM_WORKERS + 1);
    for (int i = 0; i < NUM_WORKERS; i++) {
      master_thread_args[i*NUM_WORKERS] = i;
      master_thread_args[i*NUM_WORKERS+1] = partial_hilo[0] + i * master_thread_len;
      master_thread_args[i*NUM_WORKERS+2] = min(partial_hilo[1], partial_hilo[0] + (i+1) * master_thread_len);
    } //master_t0 takes range0, t1 takes 1, t2 takes 2, self takes 3
    int master_main_thread_args[2];
    master_main_thread_args[0] = partial_hilo[0] + NUM_WORKERS * master_thread_len;
    master_main_thread_args[1] = min(partial_hilo[1], partial_hilo[0] + (NUM_WORKERS+1) * master_thread_len);
#ifdef DEBUG
    mtx.lock();
    cout << "master t0 computing from " << master_main_thread_args[0] << master_main_thread_args[1] << endl;
    mtx.unlock();
#endif
    // start threads to 1. do some work as well
    // 2. receive results from other nodes (machines)
    pthread_t threads[NUM_WORKERS];
    for (int i = 0; i < NUM_WORKERS; i++) {
      pthread_create(&threads[i], NULL, threadFn, (void*)(&master_thread_args[i*3]));
    }

#ifdef DEBUG
    myfile << "Start sending work\n";
#endif

    for (int dest = 1; dest < size; dest++) {
      MPI::COMM_WORLD.Send(&partial_hilo[(dest)*2], 2, MPI::INT, dest, 0);
    }

#ifdef DEBUG
    myfile << "Send done\n";
#endif
    
    // Master do its own share of work, partial_hilo[0,1]
    int master_t0_result = 0;
    for (int i = master_main_thread_args[0]; i < master_main_thread_args[1]; i++)
      if (isPrime(i)) master_t0_result ++;
    //for (int i = partial_hilo[0]; i < partial_hilo[1]; i++)
      //if (isPrime(i)) master_t0_result ++;
    
    // wait for work to be done
    for (int i = 0; i < NUM_WORKERS; i++ ) {
      pthread_join(threads[i], NULL);
      cout << master_thread_args[i*3] << " ";
    }

    int final_result = master_t0_result;
    for (int i = 0; i < NUM_WORKERS; i++)
      final_result += master_thread_args[i*3];
    for (int i = 0; i < NUM_WORKERS; i++)
      final_result += partial_results[i];

    cout << "Final result is " << final_result << endl;


#ifdef TIMECHECK
  auto end = steady_clock::now();
  //double elapsed_secs = double(t2 - t1); // / CLOCKS_PER_SEC;
  double elapsed_secs = ((end - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
  cout<<"Time used in secs:" << elapsed_secs <<endl;
#endif
    
    myfile << "Master finshed work\n" ;

#ifdef CORRCHECK

#endif
    
  }
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
  else { //worker

    //Receive arguments from master
    MPI::Status status;

    int worker_hilo[2];
    MPI::COMM_WORLD.Recv(worker_hilo, 2, MPI::INT, 0, MPI_ANY_TAG, status);

    //Do work
    int worker_thread_args[NUM_WORKER_THREADS*3]; //{[t0: tid,lo,hi], [t1], [t2]}. results written into idx0,3,6
    int worker_thread_len = (worker_hilo[1] - worker_hilo[0] + NUM_WORKER_THREADS + 1 - 1) / (NUM_WORKER_THREADS + 1);
    for (int i = 0; i < NUM_WORKER_THREADS; i++) {
      worker_thread_args[i*NUM_WORKER_THREADS] = i - NUM_WORKER_THREADS;
      worker_thread_args[i*NUM_WORKER_THREADS+1] = worker_hilo[0] + i * worker_thread_len;
      worker_thread_args[i*NUM_WORKER_THREADS+2] = min(worker_hilo[1], worker_hilo[0] + (i+1) * worker_thread_len);
    } //master_t0 takes range0, t1 takes 1, t2 takes 2, self takes 3
    int worker_main_thread_args[2];
    worker_main_thread_args[0] = worker_hilo[0] + NUM_WORKER_THREADS * worker_thread_len;
    worker_main_thread_args[1] = min(worker_hilo[1], worker_hilo[0] + (NUM_WORKER_THREADS+1) * worker_thread_len);

#ifdef DEBUG
    mtx.lock();
    cout << "worker starts to create threads" << endl;
    mtx.unlock();
#endif
    pthread_t worker_threads[NUM_WORKER_THREADS];
    for (int i = 0; i < NUM_WORKERS; i++) {
      pthread_create(&worker_threads[i], NULL, threadFn, (void*)(&worker_thread_args[i*3]));
    }



    int worker_main_result = 0;
    for (int i = worker_main_thread_args[0]; i < worker_main_thread_args[1]; i++) 
      if (isPrime(i)) worker_main_result ++;
    /*for (int i = worker_hilo[0]; i < worker_hilo[1]; i++) 
      if (isPrime(i)) worker_main_result ++;*/

    for (int i = 0; i < NUM_WORKER_THREADS; i++ ) {
      pthread_join(worker_threads[i], NULL);
      //cout << master_thread_args[i*3] << " ";
    }

#ifdef DEBUG
    mtx.lock();
    cout << "worker joined threads" << endl;
    mtx.unlock();
#endif

    int worker_result = worker_main_result;
    for (int i = 0; i < NUM_WORKER_THREADS; i++)
      worker_result += worker_thread_args[i*3];
    
    //Send back results
    MPI::COMM_WORLD.Send(&worker_result, 1, MPI::INT, 0, 0);
    myfile << "sent from node " << rank <<endl;

  }
  MPI::Finalize();
  myfile.close();
  
  return 0;
}