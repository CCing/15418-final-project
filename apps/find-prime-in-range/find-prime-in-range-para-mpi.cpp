#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <time.h>
#include <vector>
#include <algorithm>
#include <utmpx.h>
#include <math.h>
#include <chrono>

using namespace std;
using std::chrono::steady_clock;

#define TIMECHECK
//#define CORRCHECK
#define NUM_WORKERS 3
int partial_results[NUM_WORKERS];

ofstream myfile;


bool isPrime(int n) {
  int i;
  int nsqrt = sqrt(n) + 1;
  for(i=2; i <= nsqrt; ++i) {
    if(n%i==0) return true;
  }
  return false;
}


void *recvResponse(void *arg) {
  int recvNode = (int)(int64_t)arg + 1;
  MPI::Status status;
  // Assuming round robin static allocation, and response in the same order as sending data.
  
  MPI::COMM_WORLD.Recv(&partial_results[recvNode-1], 1, MPI::INT, recvNode, MPI::ANY_TAG, status);
  myfile << "Received from node "<< recvNode << endl;
  return NULL;
}


int main(int argc, char** argv)
{
  if (argc != 3) {
    cout << "Please input [lower, higer) " << endl;
    exit(123);
  }
  int lo = atoi(argv[1]);
  int hi = atoi(argv[2]);
  //input_args[0] = lo;
  //input_args[1] = hi;

  int provided;
  MPI_Init_thread(NULL, NULL, MPI_THREAD_MULTIPLE, &provided);
  //MPI_Init();
  //printf("Main started\n");
  cout << "Main started\n" << endl;
  if (provided != MPI_THREAD_MULTIPLE)
  {
    printf("Sorry, this MPI implementation does not support multiple threads\n");
  }
  int rank = MPI::COMM_WORLD.Get_rank();
  int size = MPI::COMM_WORLD.Get_size();
  char name[256];
  int len;
  MPI::Get_processor_name(name, len);
  
  printf("Hi I'm %s on core %d:%d\n", name, sched_getcpu(),rank);
  
  myfile.open ("mpi-log");
  
  /*MPI::Finalize();
  myfile.close();
  
  return 0;*/

  // Master
  if (rank == 0) {

#ifdef TIMECHECK
  auto start = steady_clock::now();
#endif

    // Divide work ranges. First divide into (NUM_WORKERS + 1) shares
    int partial_hilo[(NUM_WORKERS + 1) * 2]; //([lo0, hi0],[lo1, hi1])
    int p_len;
    p_len = (hi - lo + NUM_WORKERS + 1 - 1) / (NUM_WORKERS + 1);
    for (int i = 0; i < NUM_WORKERS + 1; i ++) {
      partial_hilo[i*2] = lo + p_len * i;
      partial_hilo[i*2+1] = min(partial_hilo[i*2] + p_len, hi);
    }
    
    // start threads to receive results from other nodes (machines)
    pthread_t threads[NUM_WORKERS];
    int threadIds[NUM_WORKERS];
    for (int i = 0; i < NUM_WORKERS; i++) {
      threadIds[i] = i;
      pthread_create(&threads[i], NULL, recvResponse, (void*)(int64_t)i);
    }
    
    myfile << "Start sending work\n";
    // Send work, static allocation: round robin
    for (int dest = 1; dest < size; dest++) {
      //printf("new bucket\n");
      //int sizeToSend = buckets[bucket].size();
      //printf("send size: %d\n", sizeToSend);
      MPI::COMM_WORLD.Send(&partial_hilo[(dest)*2], 2, MPI::INT, dest, 0);
      //MPI::COMM_WORLD.Send(array)
      //MPI::COMM_WORLD.Send(&(buckets[bucket].size()), 1, MPI::INT, dest, 0);
      //printf("send array\n");
      //MPI::COMM_WORLD.Send(&(buckets[bucket][0]), sizeToSend, MPI::INT, dest, 0);
      //printf("send bucket done\n");
      //bucket++;
    }
    myfile << "Send done\n";
    
    // Master do its own share of work, partial_hilo[0,1]
    int master_result = 0;
    for (int i = partial_hilo[0]; i < partial_hilo[1]; i++) 
      if (isPrime(i)) master_result ++;
    
    // wait for work to be done
    for (int i = 0; i < 3; i++ ) {
      pthread_join(threads[i], NULL);
    }

    int final_result = master_result;
    for (int i = 0; i < NUM_WORKERS; i++) 
      final_result += partial_results[i];

    cout << "result is " << final_result << endl;


#ifdef TIMECHECK
  auto end = steady_clock::now();
  //double elapsed_secs = double(t2 - t1); // / CLOCKS_PER_SEC;
  double elapsed_secs = ((end - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
  cout<<"Time used in secs:" << elapsed_secs <<endl;
#endif
    
    myfile << "Master finshed work\n" ;

#ifdef CORRCHECK

#endif
    
  }
  else {
    // On each worker node, there needs a thread receiving the data and another sorting the data and sending back.
//    //myfile << "Writing this to a file from worker.\n";
//    int sum = 0;
//    for(int i=0; i<ARRSIZE; i++) {
//      data[i] =  i * 3 / 3;
//      sum = sum + data[i];
//    }
//    data[ARRSIZE / 2] = 500;
//    
//    int val = rank + 10;
//    printf("%s:%d sending %d...\n", name, rank, val);
//    t1=clock(); //
//    MPI::COMM_WORLD.Send(&val, 1, MPI::INT, 0, 0);
//    t2=clock(); //
//    double elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
//    cout<<"(WOR)Time used in secs:" << elapsed_secs <<endl;
//    printf("%s:%d sent %d\n", name, rank, val);
//    
//    MPI::COMM_WORLD.Send(&data, ARRSIZE, MPI::INT, 0, 0);
//    
//    //printf("%d\n", data[ARRSIZE / 2] );
//    myfile << data[ARRSIZE / 2] ;

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

    //Receive
    MPI::Status status;
    //int sizes[bucketsPerNode];
    //int *recvArrPtrs[bucketsPerNode];
    //for (int i = 0; i < bucketsPerNode; i++) {
    //MPI::COMM_WORLD.Recv(&sizes[i], 1, MPI::INT, 0, MPI_ANY_TAG, status);
    //recvArrPtrs[i] = (int*)malloc(sizeof(int) * sizes[i]);
    /*if (recvArrPtrs[i] == NULL) {
      cout << "Malloc failed" << endl;
      MPI_Abort(MPI_COMM_WORLD, 666);
      exit(1);
    }*/
    int worker_hilo[2];
    MPI::COMM_WORLD.Recv(worker_hilo, 2, MPI::INT, 0, MPI_ANY_TAG, status);
    //}
    //vector<int> vectorArr[bucketsPerNode];

    //Do work
    /*for (int i = 0; i < bucketsPerNode; i++) {
      //vector<int> v(recvArrPtrs[i], &(recvArrPtrs[i][sizes[i]]));
      //std::sort(&recvArrPtrs[i][0], &recvArrPtrs[i][sizes[i]] );
      quickSort(&recvArrPtrs[i][0], 0, sizes[i]-1);
    }*/
    int worker_result = 0;
    for (int i = worker_hilo[0]; i < worker_hilo[1]; i++) 
      if (isPrime(i)) worker_result ++;
    
    //Send back results
    //for (int i = 0; i < bucketsPerNode; i++) {
    MPI::COMM_WORLD.Send(&worker_result, 1, MPI::INT, 0, 0);
    myfile << "sent from node " << rank <<endl;
    //}

  }
  MPI::Finalize();
  myfile.close();
  
  return 0;
}