from subprocess import call
import sys, os

with open(".send-a-file-log", "r") as fin:
	oldDest = fin.readline().strip()
	oldF = fin.readline().strip()

choose = raw_input("Send to [%s] with file [%s] ? \nType y/n: "% (oldDest, oldF))

if "y" == choose:
	dest = oldDest
	f = oldF
else:
	dest = raw_input("Send to which nodes? 2/3/4/5 \n ")
	f = raw_input("Which file? \n ")


if "2" in dest:
	call(["scp", f, "pi@169.254.0.2:/home/pi/418proj"])
if "3" in dest:
	call(["scp", f, "pi@169.254.0.3:/home/pi/418proj"])
if "4" in dest:
	call(["scp", f, "pi@169.254.0.4:/home/pi/418proj"])
if "5" in dest:
	call(["scp", f, "pi@169.254.0.5:/home/pi/418proj"])

with open(".send-a-file-log", "w") as fout:
	fout.write(dest)
	fout.write("\n")
	fout.write(f)