# run on Node 2 as echo server

'''
    Simple udp socket server
    Silver Moon (m00n.silv3r@gmail.com)
'''
 
import socket
import sys
 
HOST = ''   # Symbolic name meaning all available interfaces
PORT = 8888 # Arbitrary non-privileged port

f = open('/home/pi/418proj/log', 'w')
f.write("Running...\n")

# Datagram (udp) socket
try :
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    f.write('Socket created\n')
except socket.error, msg :
    f.write('Failed to create socket. Error Code : ' + str(msg[0]) + ' Message ' + msg[1] + '\n')
    f.close()
    sys.exit()
 
 
# Bind socket to local host and port
try:
    s.bind((HOST, PORT))
except socket.error , msg:
    f.write('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1] + '\n')
    f.close()
    sys.exit()
     
f.write('Socket bind complete\n')

 
#now keep talking with the client
while True:
    f.write("Trying to receive...\n")
    f.flush()
    # receive data from client (data, addr)
    d = s.recvfrom(1024)

    data = d[0]
    addr = d[1] 
    if not data: 
        break
     
    reply = 'OK...' + data
    f.write('Message[' + addr[0] + ':' + str(addr[1]) + '] - ' + data.strip() + '\n')
    f.flush()

    #s.sendto(reply , addr)
    #while True:
    try:
        s.sendto(reply , addr)
    except socket.error, msg:
        #f.write("Unexpected error:" + str(sys.exc_info()[0]) + "\n")
        f.write('Failed to send msg. Error Code : ' + str(msg[0]) + ' Message ' + msg[1] + '\n')
        f.flush()
        #continue
        f.close()
        sys.exit()
        #break
    except:
        f.write("Other error " + str(sys.exc_info()[0]))
        f.close()
        sys.exit()

    f.write("Message sent\n") 
    f.flush()
    
f.close()
s.close()