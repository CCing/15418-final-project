# run on Node 2 as echo server

'''
    Simple udp socket server
    Silver Moon (m00n.silv3r@gmail.com)
'''
 
import socket
import sys, fcntl, os, errno
from time import sleep

addr2 = ('169.254.0.2', 8888)
addr3 = ('169.254.0.3', 8888)
addr4 = ('169.254.0.4', 8888)
addr5 = ('169.254.0.5', 8888)
 
HOST = ''   # Symbolic name meaning all available interfaces
PORT = 8888 # Arbitrary non-privileged port

f = open('/home/pi/418proj/log', 'w')
f.write("Running...\n")

# Datagram (udp) socket
try :
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    f.write('Socket created\n')
except socket.error, msg :
    f.write('Failed to create socket. Error Code : ' + str(msg[0]) + ' Message ' + msg[1] + '\n')
    f.close()
    sys.exit()
 
 
# Bind socket to local host and port
try:
    s.bind((HOST, PORT))
except socket.error , msg:
    f.write('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1] + '\n')
    f.close()
    sys.exit()
 
fcntl.fcntl(s, fcntl.F_SETFL, os.O_NONBLOCK)


f.write('Socket bind complete\n')

dataCount = 0
#now keep talking with the client
while True:
    #f.write("Trying to receive...\n")
    f.flush()
    # receive data from client (data, addr)
    try:
        data, addrPC = s.recvfrom(8192)
    except socket.error as e:
        if e.errno == errno.EAGAIN : 
            #sleep(0.5)
            continue
        # treat anything else as fatal error
        f.write('Failed to recv data. Error Code : ' + str(msg[0]) + ' Message ' + msg[1] + '\n')
        sys.exit()


    if not data: 
        break
    # f.write('Message from PC [' + addrPC[0] + ':' + str(addrPC[1]) + '] - ' + '\n')#+ data.strip() + '\n')
    # f.flush()

    dataCount += len(data.split())
    # f.write("Have received count: " + str(dataCount) + "\n")

    # msg = "Test to worker nodes" 
    # s.sendto(msg, addr3)
    # s.sendto(msg, addr4)
    # s.sendto(msg, addr5)
    
    # f.write('Receiving responses from workers...\n')
    # f.flush()

    # d = s.recvfrom(1024)

    # data = d[0]
    # addr = d[1] 
    # if not data: 
    #     break
    # f.write('Message from worker [' + addr[0] + ':' + str(addr[1]) + '] - ' + data.strip() + '\n')
    # f.flush()

    # d = s.recvfrom(1024)

    # data = d[0]
    # addr = d[1] 
    # if not data: 
    #     break
    # f.write('Message from worker [' + addr[0] + ':' + str(addr[1]) + '] - ' + data.strip() + '\n')
    # f.flush()

    # d = s.recvfrom(1024)

    # data = d[0]
    # addr = d[1] 
    # if not data: 
    #     break
    # f.write('Message from worker [' + addr[0] + ':' + str(addr[1]) + '] - ' + data.strip() + '\n')
    # f.flush()


    # f.write("Got all responses\n")
    # f.flush()

    if (dataCount == 10000000):
        dataCount = 0
        reply = 'OK...'
        



        #s.sendto(reply , addr)
        #while True:
        try:
            s.sendto(reply , addrPC)
        except socket.error, msg:
            #f.write("Unexpected error:" + str(sys.exc_info()[0]) + "\n")
            f.write('Failed to send msg. Error Code : ' + str(msg[0]) + ' Message ' + msg[1] + '\n')
            f.flush()
            #continue
            f.close()
            sys.exit()
            #break
        except:
            f.write("Other error " + str(sys.exc_info()[0]))
            f.close()
            sys.exit()

        f.write("ACK message sent\n") 
        f.flush()
    
f.close()
s.close()

