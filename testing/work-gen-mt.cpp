//Quicksort, http://www.algolist.net/Algorithms/Sorting/Quicksort

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <vector>
#include <algorithm>
#include <utmpx.h>
//#include <pthread.h>
//#define ARRLEN 100000
using namespace std;

#define TIMECHECK
#define CORRCHECK
#define NUM_OF_CHUNKS 16
#define NUM_OF_THREADS 4
int ARRLEN = 0;

int chunkSize = 1073741824 / (NUM_OF_CHUNKS / 2);
int *arr;

struct Bucket {
  vector<int> vec;
  //mutex m;
  void put(int val) {
    //m.lock();
    vec.push_back(val);
    //m.unlock();
  }
};

Bucket* buckets[NUM_OF_CHUNKS];


void *doBucketWork(void *arg) {
  int id = (int)(int64_t)arg;
  printf("Running thread on core %d\n", sched_getcpu());
//  int start = id * (ARRLEN / NUM_OF_THREADS);
//  int end = (id + 1) * (ARRLEN / NUM_OF_THREADS);
//
//  for (int i = start; i < end; i++) {
//    int value = arr[i];
//    int bucket = value / chunkSize;
//    buckets[bucket]->put(value);
//  }
  int bsize = NUM_OF_CHUNKS / NUM_OF_THREADS;

  for (int i = 0; i < ARRLEN; i++) {
    int value = arr[i];
    int bucket = value / chunkSize;
    if (bucket / bsize == id) {
      buckets[bucket]->put(value);
    }
  }
  return NULL;
}


int main(int argc, char *argv[]) {
  ARRLEN = atoi(argv[1]);
  srand (time(0));
  for (int i = 0; i < NUM_OF_CHUNKS; i++) {
    buckets[i] = new Bucket();
  }
  
  arr = (int *)malloc(sizeof(int) * ARRLEN);

  for (int i = 0; i < ARRLEN; i++) {
    arr[i] = std::rand();
  }

#ifdef TIMECHECK
  clock_t t1,t2;
  t1=clock();
#endif

  cout << "RAND MAX IS " << RAND_MAX << endl;
  
  /* Do parallel bucket jobs */
  pthread_t threads[NUM_OF_THREADS];
  //int tids[NUM_OF_THREADS];
  for (int i = 0; i < NUM_OF_THREADS; i++ ) {
    //tids[i] = i;
    pthread_create(&threads[i], NULL, doBucketWork, (void*)(int64_t)i);
  }

  for (int i = 0; i < NUM_OF_THREADS; i++ ) {
    pthread_join(threads[i], NULL);
  }
  
  /* Do sequential bucket work */
//  for (int i = 0; i < ARRLEN; i++) {
//    int value = arr[i];
//    int bucket = value / chunkSize;
//    buckets[bucket]->vec.push_back(value);
//  }


#ifdef TIMECHECK
  t2=clock();
  //float diff ((float)t2-(float)t1);
  double elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
  cout<<"Time used in secs:" << elapsed_secs <<endl;
#endif
  
  int sum = 0;
  for (int i = 0; i < NUM_OF_CHUNKS; i++) {
    sum += buckets[i]->vec.size();
  }
  cout << "SUM: " << sum << endl;
  
//  //quickSort(arr, 0, ARRLEN);
//  std::vector<int> v(arr, &arr[ARRLEN]);
//  std::sort(v.begin(), v.end());
//
//#ifdef TIMECHECK
//  t2=clock();
//  //float diff ((float)t2-(float)t1);
//  double elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
//  cout<<"Time used in secs:" << elapsed_secs <<endl;
//#endif
//
//#ifdef CORRCHECK
//  int left, right;
//  left = -1;
//  right = -1;
//  for (auto it = v.begin() ; it != v.end(); ++it) {
//    if (left != -1 && right != -1 && left > right) {
//      cout << "not sorted" << endl;
//      exit(1);
//    } 
//    left = right;
//    right = *it;
//  }
//  cout << "yes, it's sorted" << endl;
//
//#endif

  free(arr);

  return 0;
}
