# run on pc to start the whole process
'''
    udp socket client
    Silver Moon
'''
 
import socket   #for sockets
import sys  #for exit
import time #for time
import random #for random generator
 
# create dgram udp socket
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
except socket.error:
    print 'Failed to create socket'
    sys.exit()
 
host = '169.254.0.2'
port = 8888


# Bind socket to local host and port
myHOST = '169.254.65.167' #follows computer, varies from diff computers
myPORT = 8889
try:
    s.bind((myHOST, myPORT))
except socket.error , msg:
    print ('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1] + '\n')
    sys.exit()


random.seed()
buf = []
for i in xrange(1000):
    num = random.randint(0, 9999999)
    buf.append(str(num))

buf = " ".join(n for n in buf)

#print buf

count = 0
start = time.time()


while(count < 1) :
    #msg = 'Test start' #raw_input('Enter message to send : ')
     
    try :
        #Set the whole string
        for i in xrange(10000000/1000):
            if (i % 14 == 0):
                time.sleep(0.008)
            s.sendto(buf, (host, port))
         
        print "Buffer sent"
        # receive data from client (data, addr)
        d = s.recvfrom(1024)
        reply = d[0]
        addr = d[1] 

        #print 'Server reply from : ' + addr[0] + ':' + str(addr[1])
     
    except socket.error, msg:
        print 'Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        sys.exit()

    count += 1

end = time.time()

print "Time elapsed on messaging: ", end-start