//Quicksort, http://www.algolist.net/Algorithms/Sorting/Quicksort

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <vector>
#include <chrono>
#include <algorithm>
#include <pthread.h>
//#define ARRLEN 100000
using namespace std;
using std::chrono::steady_clock;
#define TIMECHECK
#define NUM_THREADS 4
//#define CORRCHECK


int NUM_OF_BUCKETS;
int bucketRange;
int ARRLEN;
int *arr;
vector<vector<short>> buckets;

vector<vector<short>> myBuckets[NUM_THREADS];
pthread_barrier_t barrier;



void *doBuckets(void* args) {
  int id = (int)args;
  //vector<short> myBuckets[NUM_OF_BUCKETS];
  int chunk = ARRLEN / NUM_THREADS;

  pthread_barrier_wait(&barrier);

  for (int i = id * chunk; i < (id + 1) * chunk; i++) {
    int value = arr[i];
    int bucketI = value / bucketRange;
    if (bucketI >= NUM_OF_BUCKETS) bucketI = NUM_OF_BUCKETS-1;
    myBuckets[id][bucketI].push_back((short)value);
  }
  
  pthread_barrier_wait(&barrier);
  int parts = NUM_OF_BUCKETS / NUM_THREADS;
  for (int i = id * parts; i < (id+1) * parts; i++) {
    for (int j = 0; j < NUM_THREADS; j++) {
      buckets[i].insert(buckets[i].end(), myBuckets[j][i].begin(), myBuckets[j][i].end());
    }
  }
  
  pthread_barrier_wait(&barrier);

  return NULL;
}


int main(int argc, char *argv[]) {
  int ARRLEN = atoi(argv[1]);
  NUM_OF_BUCKETS = atoi(argv[2]);
  bucketRange = 1073741824 / (NUM_OF_BUCKETS / 2);
  buckets.resize(NUM_OF_BUCKETS, *(new vector<short>));
  for (int i = 0; i < NUM_THREADS; i++) {
    myBuckets[i].resize(NUM_OF_BUCKETS, *(new vector<short>));
  }

  srand (time(0));
  
  pthread_barrier_init(&barrier, NULL, 4);

  arr = (int *)malloc(sizeof(int) * ARRLEN);

  for (int i = 0; i < ARRLEN; i++) 
    arr[i] = std::rand();

  pthread_t thread[3];
  pthread_create(&thread[0], NULL, doBuckets, (void*)0);
  pthread_create(&thread[1], NULL, doBuckets, (void*)1);
  pthread_create(&thread[2], NULL, doBuckets, (void*)2);
  
#ifdef TIMECHECK
//  clock_t t1,t2;
//  t1=clock();
  auto start = steady_clock::now();
  cout << "Timer start" << endl;
#endif

  /* Divide to chunks
  for (int i = 0; i < ARRLEN; i++) {
    int value = arr[i];
    int bucketI = value / bucketRange;
    if (bucketI >= NUM_OF_BUCKETS) bucketI = NUM_OF_BUCKETS-1;
    buckets[bucketI].push_back((short)value);
  }*/
  
  // parallel divide
  pthread_barrier_wait(&barrier);
  int chunk = ARRLEN / NUM_THREADS;
  for (int i = 3 * chunk; i < 4 * chunk; i++) {
    int value = arr[i];
    int bucketI = value / bucketRange;
    if (bucketI >= NUM_OF_BUCKETS) bucketI = NUM_OF_BUCKETS-1;
    myBuckets[3][bucketI].push_back((short)value);
  }
  
  pthread_barrier_wait(&barrier);
  free(arr);
  
  int parts = NUM_OF_BUCKETS / NUM_THREADS;
  
  for (int j = 0; j < NUM_THREADS; j++) {
    for (int i = 3 * parts; i < 4 * parts; i++) {
      buckets[i].insert(buckets[i].end(), myBuckets[j][i].begin(), myBuckets[j][i].end());
    }
  }  

  pthread_barrier_wait(&barrier);
  //quickSort(arr, 0, ARRLEN);
  //std::sort(arr, arr+ARRLEN);
  
  for (int i = 0; i < NUM_THREADS; i++) {
    for (int j = 0; j < NUM_OF_BUCKETS; j++) {
      myBuckets[i][j].clear();
    }
    myBuckets[i].clear();
  }

#ifdef TIMECHECK
  //t2=clock();
  //float diff ((float)t2-(float)t1);
  //double elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
  auto curr = steady_clock::now();
  double elapsed_secs = ((curr - start).count()) * steady_clock::period::num / static_cast<double>(steady_clock::period::den);
  cout<<"Time used in secs:" << elapsed_secs <<endl;
#endif

  //free(arr);

  return 0;
}
