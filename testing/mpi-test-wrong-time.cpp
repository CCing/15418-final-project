#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <time.h> 
using namespace std;

#define ARRSIZE 1000000

int data[ARRSIZE];

int main(int argc, char** argv)
{
    MPI::Init();
    int rank = MPI::COMM_WORLD.Get_rank();
    int size = MPI::COMM_WORLD.Get_size();
    char name[256];
    int len;
    MPI::Get_processor_name(name, len);

    printf("Hi I'm %s:%d\n", name, rank);
      ofstream myfile;

  myfile.open ("mpi-log");

  clock_t t1,t2;



    if (rank == 0) {

            //myfile << "Writing this to a file from MASTER.\n";
MPI::Status status;
        while (size > 1) {
            int val;
              t1=clock(); //
            MPI::COMM_WORLD.Recv(&val, 1, MPI::INT, MPI::ANY_SOURCE, MPI::ANY_TAG, status);
            t2=clock(); //
            int source = status.Get_source();
            printf("%s:0 received %d from %d\n", name, val, source);
            size--;
  double elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
  cout<<"(MAS)Time used in secs:" << elapsed_secs <<endl;
        }
        printf("all workers checked in!\n");
        MPI::COMM_WORLD.Recv(&data, ARRSIZE, MPI::INT, MPI::ANY_SOURCE, MPI::ANY_TAG, status);

        myfile << data[ARRSIZE / 2] ;
    }
    else {
            //myfile << "Writing this to a file from worker.\n";
    int sum = 0;
    for(int i=0; i<ARRSIZE; i++) {
      data[i] =  i * 3 / 3;
      sum = sum + data[i];
    }
    data[ARRSIZE / 2] = 500;
    
        int val = rank + 10;
        printf("%s:%d sending %d...\n", name, rank, val);
                      t1=clock(); //
        MPI::COMM_WORLD.Send(&val, 1, MPI::INT, 0, 0);
                    t2=clock(); //
  double elapsed_secs = double(t2 - t1) / CLOCKS_PER_SEC;
  cout<<"(WOR)Time used in secs:" << elapsed_secs <<endl;
        printf("%s:%d sent %d\n", name, rank, val);

        MPI::COMM_WORLD.Send(&data, ARRSIZE, MPI::INT, 0, 0);

        //printf("%d\n", data[ARRSIZE / 2] );
        myfile << data[ARRSIZE / 2] ;

    }
    MPI::Finalize();
  myfile.close();

    return 0;
}